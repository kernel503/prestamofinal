package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class GrupoAsignadoConsulActualiElim extends AppCompatActivity {

    ControlBD helper;
    EditText et1, et2,et3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_asignado_consul_actuali_elim);
        helper = new ControlBD(this);
        et1 = (EditText) findViewById(R.id.et_idGA);
        et2 = (EditText) findViewById(R.id.et_lugarAG);
        et3 = (EditText) findViewById(R.id.et_capacidadAG);

    }

    public void consultar(View v) {
        helper.abrir();
        GrupoAsignado asignado = helper.consultarGrupoAsignado(et1.getText().toString());
        helper.cerrar();
        if (asignado == null)
            Toast.makeText(this, "Grupo con id " + et1.getText().toString() + " no encontrado", Toast.LENGTH_LONG).show();
        else {
            et2.setText(asignado.getLugar());
            et3.setText(String.valueOf(asignado.getCapacidad()));

        }
    }

    public void actualizar(View v) {
        String id = et1.getText().toString();
        String lugar=et2.getText().toString();
        String capacidad =et3.getText().toString();

        if(!id.isEmpty()&&!lugar.isEmpty()&&!capacidad.isEmpty()) {
            GrupoAsignado asignado = new GrupoAsignado();
            asignado.setIdGrupo(Integer.valueOf(id));
            asignado.setLugar(lugar);
            asignado.setCapacidad(Integer.parseInt(capacidad));
            helper.abrir();
            String estado = helper.actualizarGrupoAsignado(asignado);
            helper.cerrar();
            Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, "Completar los campos", Toast.LENGTH_SHORT).show();

    }


    public void eliminar(View v) {
        String id = et1.getText().toString();
        String regEliminadas = "";
        if (!id.isEmpty()) {
            GrupoAsignado asignado = new GrupoAsignado();
            asignado.setIdGrupo(Integer.parseInt(id));
            helper.abrir();
            regEliminadas = helper.eliminarGrupoAsignado(asignado);
            helper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Debe Ingresar el id", Toast.LENGTH_SHORT).show();
        }


    }
}
