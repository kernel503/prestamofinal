package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.eisi.entrega1pdm.R;


public class MostrarArticuloOficina extends Fragment implements View.OnClickListener{
    private ListView listView;
    private Button btnEditar;
    private Button btnEliminar;
    private ImageButton btnAdd;
    private List<String> stringArrayList ;
    ArrayList<ArticuloOficina> lista = new ArrayList<>();
    private static final String Tag = "Mensajes";
    ControlBDMS12016 BDHelper;
    private Integer idArt;
    private String Nombre="";
    private Integer cantidad;
    private String distribuidor="";
    private ArrayAdapter<String> stringArrayAdapter;

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.btnAdd:
                InsertarArticuloOficinaFragment fragment = new InsertarArticuloOficinaFragment();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;

            case R.id.btnEditar:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                Context context = getContext();
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final TextView ed = new TextView(context);
                ed.setText("EDITAR REGISTRO");
                layout.addView(ed);

                final TextView nom = new TextView(context);
                nom.setText("Nombre del Articulo");
                layout.addView(nom);

                final EditText nombre = new EditText(context);
                nombre.setText(Nombre);
                layout.addView(nombre);

                final TextView cant = new TextView(context);
                cant.setText("Cantidad");
                layout.addView(cant);

                final EditText canti = new EditText(context);
                canti.setText(String.valueOf(cantidad));
                layout.addView(canti);

                final TextView dis = new TextView(context);
                dis.setText("Distribuidor");
                layout.addView(dis);

                final EditText distribuir = new EditText(context);
                distribuir.setText(distribuidor);
                layout.addView(distribuir);

                builder.setView(layout);
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tost = "";
                        Nombre = nombre.getText().toString();
                        cantidad = Integer.parseInt(canti.getText().toString());
                        distribuidor = distribuir.getText().toString();
                        ArticuloOficina articulo = new ArticuloOficina(idArt,Nombre,distribuidor,cantidad);
                        BDHelper.abrir();
                        tost = BDHelper.actualizar(articulo);
                        Toast.makeText(getActivity(), tost, Toast.LENGTH_SHORT).show();
                        BDHelper.cerrar();
                        //  db.abrir();
                        //  db.Actualizar(articulo);
                        // db.cerrar();
                        llenarTabla();

                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

                break;

            case R.id.btnEliminar:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                builder2.setCancelable(true);
                builder2.setTitle("Confirmar eliminacion");
                builder2.setMessage("Eliminar registro: "+Nombre);
                builder2.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tosta = "";
                                BDHelper.abrir();
                                ArticuloOficina ue = new ArticuloOficina(idArt,Nombre,distribuidor,cantidad);
                                tosta=BDHelper.eliminar(ue);
                                BDHelper.cerrar();
                                Toast.makeText(getActivity(),tosta,Toast.LENGTH_SHORT).show();
                                llenarTabla();
                                btnEditar.setEnabled(false);
                                btnEliminar.setEnabled(false);
                            }
                        });
                builder2.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder2.create();
                dialog.show();
                break;

        }//falta eliminar
    }
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                    Bundle savedInstanceState) {
                // Inflate the layout for this fragment
                View v = inflater.inflate(R.layout.fragment_mostrar_articulo_oficina, container, false);
                BDHelper  = new ControlBDMS12016(getActivity());

                listView=(ListView) v.findViewById(R.id.listView);
                btnAdd =(ImageButton) v.findViewById(R.id.btnAdd);
                btnEditar = (Button) v.findViewById(R.id.btnEditar);
                btnEliminar = (Button) v.findViewById(R.id.btnEliminar);

                btnEliminar.setOnClickListener(this);
                btnEditar.setOnClickListener(this);
                btnAdd.setOnClickListener(this);
                btnEditar.setEnabled(false);
                btnEliminar.setEnabled(false);

                llenarTabla();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                        Object clickItemObj = adapterView.getAdapter().getItem(index);
                        btnEditar.setEnabled(true);
                        btnEliminar.setEnabled(true);

                        idArt=lista.get(index).getIdArticulo();
                        Nombre= lista.get(index).getNombre();
                        cantidad= lista.get(index).getCantidad();
                        distribuidor=lista.get(index).getDistribuidor();

                    }
                });

                return v;
            }


            private void llenarTabla(){
                lista= BDHelper.listaArticuloOficina();
                stringArrayList= new ArrayList<>();
                for (ArticuloOficina a : lista){
                    stringArrayList.add("nombre: "+a.getNombre()+" \nCantidad: "+String.valueOf(a.getCantidad())+ "\nDistribuidor: "+a.getDistribuidor());
                }

                stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
                listView.setAdapter(stringArrayAdapter);
            }


    }
