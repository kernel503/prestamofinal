package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity2 extends ListActivity {
    String[] menu={"Tabla Documento","Tabla Autor","Tabla tipoDocumento"};
    String[] activities={"DocumentoMenuActivity","AutorMenuActivity","TipoDocumentoMenuActivity"};
    ControlBD BDhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menu));
        BDhelper = new ControlBD(this);
    }
    @Override
    protected void onListItemClick(ListView l,View v,int position,long id){
        super.onListItemClick(l, v, position, id);
        if(position!=3){
            String nombreValue=activities[position];
            try{
                Class<?> clase=Class.forName("edu.ues.fia.eisi.entrega1pdm.CM13106."+nombreValue);
                Intent inte = new Intent(this,clase);
                this.startActivity(inte);
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
        }else{
//CODIGO PARA LLENAR BASE DE DATOS
            //BDhelper.abrir();
            //String tost=BDhelper.llenarBDCarnet();
            //BDhelper.abrir(); //String tost=BDhelper.LlenarBD();
           // BDhelper.LlenarBD();
            //BDhelper.cerrar();
            //Toast.makeText(this, tost, Toast.LENGTH_SHORT).show();
        }
    }
}
