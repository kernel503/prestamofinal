package edu.ues.fia.eisi.entrega1pdm.CV15018;

public class GrupoAsignado {
    private int idGrupo;
    private  String lugar;
    private int capacidad;


    public GrupoAsignado() {

    }


    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

}
