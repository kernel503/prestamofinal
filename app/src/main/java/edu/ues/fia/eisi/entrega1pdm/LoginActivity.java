package edu.ues.fia.eisi.entrega1pdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import edu.ues.fia.eisi.entrega1pdm.oo15004.RegistroPrestamo;
import edu.ues.fia.eisi.entrega1pdm.oo15004.oo15004Principal;

public class LoginActivity extends AppCompatActivity {
    ControladorBDPrincipal BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        BDhelper=new ControladorBDPrincipal(this);
    }

    public void Comprobar(View v){
        EditText editUser = findViewById(R.id.editUser);
        EditText editPass = findViewById(R.id.editPass);
        String user = editUser.getText().toString();
        String pass = editPass.getText().toString();
        if(user.equals("1") && pass.equals("1")){
            Intent myIntent = new Intent(LoginActivity.this, Admin.class);
            startActivity(myIntent);
        }
        if (!BDhelper.usuario(user)){
            editUser.setError(getString(R.string.invalidUser));
            editUser.requestFocus();
        }
        if (!BDhelper.contra(user,pass)){
            editPass.setError(getString(R.string.wrongPass));
            editPass.requestFocus();
        }
        if (BDhelper.usuario(user) && BDhelper.contra(user,pass) ){
            BDhelper.datos(user,pass);
            Docente d = new Docente();
            //DOCENTE NORMAL  2
            //ADMINISTRADOR 1
            if (d.getAcceso()== 2){
                Intent myIntent = new Intent(LoginActivity.this, oo15004Principal.class);
                startActivity(myIntent);
            }else if (d.getAcceso()== 1){
                Intent myIntent2 = new Intent(LoginActivity.this, Admin.class);
                startActivity(myIntent2);
            }else if (d.getAcceso() == 3){
                Intent myIntent3 = new Intent(LoginActivity.this, RegistroPrestamo.class);
                startActivity(myIntent3);
            }

        }

    }
}
