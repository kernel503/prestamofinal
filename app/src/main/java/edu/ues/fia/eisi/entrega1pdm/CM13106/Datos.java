package edu.ues.fia.eisi.entrega1pdm.CM13106;

public class Datos {

    private static  final String[] insertDocumento={
            "INSERT INTO Documento (idDocumento,idTipoDocumento,idAutor,idEditorial,titulo,isbm,idioma,cantidad) VALUES('1','1','2','1','Como programar en java septima edicion','978-970-26-1190-5','Espaniol','4'); ",
            "INSERT INTO Documento (idDocumento,idTipoDocumento,idAutor,idEditorial,titulo,isbm,idioma,cantidad) VALUES('2','1','3','2','Organizacion y arquitectura de computadoras','9788489660243','Espaniol','2'); ",
            "INSERT INTO Documento (idDocumento,idTipoDocumento,idAutor,idEditorial,titulo,isbm,idioma,cantidad) VALUES('3','2','4','2','PROTECCION INTEGRAL DE LA NIÑEZ Y ADOLESCENCIA (LEPINA)','878-852-26-1120-5','Espaniol','1'); ",
            "INSERT INTO Documento (idDocumento,idTipoDocumento,idAutor,idEditorial,titulo,isbm,idioma,cantidad) VALUES('4','2','4','1','Programando en c++','978-84-9122-139-5','Espaniol','4'); ",
            "INSERT INTO Documento (idDocumento,idTipoDocumento,idAutor,idEditorial,titulo,isbm,idioma,cantidad) VALUES('5','2','2','1','Programando en c++','978-84-9122-139-6','Espaniol','28'); ",
             };

    private static  final String[] insertTipoDocumento={
            "INSERT INTO TipoDocumento (idTipoDocumento,catalogo) VALUES('1','Libro'); ",
            "INSERT INTO TipoDocumento (idTipoDocumento,catalogo) VALUES('2','Tesis'); ",
            "INSERT INTO TipoDocumento (idTipoDocumento,catalogo) VALUES('3','Reporte'); ",
    };

    private static  final String[] insertAutor={
            "INSERT INTO Autor (idAutor,nombre,nacionalidad) VALUES('1','Gabriela Azucena Campos García','Salvadoreña'); ",
            "INSERT INTO Autor (idAutor,nombre,nacionalidad) VALUES('2','Roberto Martínez Román ','Colombiano'); ",
            "INSERT INTO Autor (idAutor,nombre,nacionalidad) VALUES('3','William Stallings ','Estadounidense'); ",
            "INSERT INTO Autor (idAutor,nombre,nacionalidad) VALUES('4','MARTINEZ GOCHEZ, CESAR ERNESTO','Salvadoreño'); ",

    };


    public static String[] getInsertDocumento() {
        return insertDocumento;
    }

    public static String[] getInsertTipoDocumento() {
        return insertTipoDocumento;
    }
    public static String[] getInsertAutor() {
        return insertAutor;
    }




}
