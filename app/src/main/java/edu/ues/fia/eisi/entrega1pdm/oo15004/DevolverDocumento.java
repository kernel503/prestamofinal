package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DevolverDocumento extends Fragment implements View.OnClickListener {
    View v;
    Docente d = new Docente();
    int idDocente = d.getIdDocente();
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    FloatingActionButton btnDel;
    Spinner spinner;
    ArrayList<Documento> lista = new ArrayList<>();
    ArrayList<TipoDocumento> listaSpinner = new ArrayList<>();
    int idPrestamoDoc = 0;
    String nombre ="";
    String idTipoDocumento = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_devolver_documento, container, false);
        BDhelper=new ControladorBD(getActivity());
        listView = v.findViewById(R.id.listaDevolucionDocumento);
        btnDel = v.findViewById(R.id.devolverDocumento);
        spinner = (Spinner) v.findViewById(R.id.spDocumento);
        btnDel.setOnClickListener(this);
        btnDel.setEnabled(false);

        //llenarTabla();
        llenarSpinner();

        //CLICK LISTVIEW
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lista.isEmpty()){
                    idPrestamoDoc = lista.get(index).getIdPrestamoDocumento();
                    nombre = lista.get(index).getNombreDocumento();
                    btnDel.setEnabled(true);
                }
            }
        });

        //CLICK SPINNER
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        idTipoDocumento=""+listaSpinner.get(pos).getIdTipoDocumento();
                        llenarTabla();
                        if (lista.isEmpty()){
                            btnDel.setEnabled(false);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


        return v;
    }

    public void llenarSpinner(){
        listaSpinner = BDhelper.spinnerTipoDoc();
        stringArrayList= new ArrayList<>();
        for (TipoDocumento a : listaSpinner){
            stringArrayList.add(a.getCatalogo());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(),   android.R.layout.simple_spinner_item, stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    private void llenarTabla(){
        lista = BDhelper.listaPrestamoDocumento(idDocente,idTipoDocumento);
        stringArrayList= new ArrayList<>();

        if (!lista.isEmpty()){
            for (Documento a : lista){
                stringArrayList.add(a.getNombreDocumento()+"\n"+a.getNombreAutor()
                        +"\n"+a.getNombreEditorial()+" / "+a.getIsbm());
            }
        }else {
            stringArrayList.add(getString(R.string.listEmpty));
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.devolverDocumento:
                Documento d = new Documento();
                d.setIdPrestamoDocumento(idPrestamoDoc);
                d.setNombreDocumento(nombre);
                Snackbar.make(getView(), BDhelper.devolverDocumento(d), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                btnDel.setEnabled(false);
        }

    }
}
