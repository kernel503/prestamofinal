package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class MateriaConsulActualiElim extends AppCompatActivity {
    ControlBD helper;
    EditText et1, et2, et3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_consul_actuali_elim);
        helper = new ControlBD(this);
        et1 = (EditText) findViewById(R.id.et_idMat);
        et2 = (EditText) findViewById(R.id.et_codMat);
        et3 = (EditText) findViewById(R.id.et_nomMat);
    }

    public void consultar(View v) {
        helper.abrir();
        Materia materia = helper.consultarMateria(et1.getText().toString());
        helper.cerrar();
        if (materia == null)
            Toast.makeText(this, "Materia con id " + et1.getText().toString() + " no encontrado", Toast.LENGTH_LONG).show();
        else {
            et2.setText(materia.getCodMateria());
            et3.setText(materia.getNombre());

        }
    }

    public void actualizar(View v) {
        Materia materia = new Materia();
        materia.setIdMateria(Integer.valueOf(et1.getText().toString()));
        materia.setCodMateria(et2.getText().toString());
        materia.setNombre(et3.getText().toString());
        helper.abrir();
        String estado = helper.actualizarMateria(materia);
        helper.cerrar();
        Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
    }

    public void eliminar(View v) {
        String id = et1.getText().toString();
        String regEliminadas = "";
        if (!id.isEmpty()) {
            Materia materia = new Materia();
            materia.setIdMateria(Integer.parseInt(id));
            helper.abrir();
            regEliminadas = helper.eliminarMateria(materia);
            helper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
            et1.setText("");
            et2.setText("");
            et3.setText("");
        } else {
            Toast.makeText(this, "Debe Ingresar el ID", Toast.LENGTH_SHORT).show();
        }


    }
}

