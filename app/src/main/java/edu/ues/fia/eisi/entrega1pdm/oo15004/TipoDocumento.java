package edu.ues.fia.eisi.entrega1pdm.oo15004;

public class TipoDocumento {
    int idTipoDocumento;
    String catalogo;

    public TipoDocumento() {
    }

    public TipoDocumento(int idTipoDocumento, String catalogo) {
        this.idTipoDocumento = idTipoDocumento;
        this.catalogo = catalogo;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }
}
