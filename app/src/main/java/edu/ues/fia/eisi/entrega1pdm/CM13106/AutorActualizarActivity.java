package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class AutorActualizarActivity extends Activity {

    ControlBD helper;
    EditText editIdAutor;
    EditText editNombre;
    EditText editNacionalidad;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_actualizar);
        helper = new ControlBD(this);
        editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        editNombre = (EditText) findViewById(R.id.editNombre);
        editNacionalidad = (EditText) findViewById(R.id.editNacionalidad);
    }
    public void actualizarAutor(View v) {
        Autor autor = new Autor();
        autor.setIdAutor(Integer.parseInt(editIdAutor.getText().toString()));
        autor.setNombre(editNombre.getText().toString());
        autor.setNacionalidad(editNacionalidad.getText().toString());
        helper.abrir();
        String estado = helper.actualizar(autor);
        helper.cerrar();
        Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
    }
    public void limpiarTexto(View v) {
        editIdAutor.setText("");
        editNombre.setText("");
        editNacionalidad.setText("");
    }
}
