package edu.ues.fia.eisi.entrega1pdm.oo15004;

public class ArticuloOficina {
    int idArticulo, cantidad;
    String nombre;
    int idDocente, idPrestamoArticulo;

    public int getIdPrestamoArticulo() {
        return idPrestamoArticulo;
    }

    public void setIdPrestamoArticulo(int idPrestamoArticulo) {
        this.idPrestamoArticulo = idPrestamoArticulo;
    }

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public ArticuloOficina() {
    }

    public ArticuloOficina(int idArticulo, String nombre, int cantidad) {
        this.idArticulo = idArticulo;
        this.cantidad = cantidad;
        this.nombre = nombre;
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
