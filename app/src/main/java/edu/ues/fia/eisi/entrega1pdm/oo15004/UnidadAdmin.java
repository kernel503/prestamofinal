package edu.ues.fia.eisi.entrega1pdm.oo15004;

public class UnidadAdmin {
    int idUnidadAdmin;
    String escuela, unidad;

    public UnidadAdmin() {
    }

    public int getIdUnidadAdmin() {
        return idUnidadAdmin;
    }

    public void setIdUnidadAdmin(int idUnidadAdmin) {
        this.idUnidadAdmin = idUnidadAdmin;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }
}
