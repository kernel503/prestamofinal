package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;

public class SolicitarDocumento extends Fragment implements View.OnClickListener {

    Docente d = new Docente();
    int idDocente = d.getIdDocente();


    private static final String Tag = "Mensajes";
    View v;
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;

    private ListView listView;

    Spinner spinner;
    ArrayList<TipoDocumento> listaSpinner = new ArrayList<>();
    ArrayList<Documento> lista = new ArrayList<>();
    String idTipoDocumento = "";
    int idDocumento;
    String nombreDoc="";

    FloatingActionButton btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_solicitar_documento, container, false);
        BDhelper=new ControladorBD(getActivity());

        spinner = (Spinner) v.findViewById(R.id.spinnerDoc);
        listView = v.findViewById(R.id.listaSolDoc);
        btnAdd = v.findViewById(R.id.agregarDocumento);
        btnAdd.setEnabled(false);
        btnAdd.setOnClickListener(this);
        llenarSpinner();

        //CLICK SPINNER
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        idTipoDocumento=""+listaSpinner.get(pos).getIdTipoDocumento();
                        llenarTabla();
                        if (lista.isEmpty()){
                            btnAdd.setEnabled(false);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        //CLICK LISTVIEW
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lista.isEmpty()){
                    idDocumento = lista.get(index).getIdDocumento();
                    nombreDoc = lista.get(index).getNombreDocumento();
                    Log.i(Tag, ""+idDocumento);
                    btnAdd.setEnabled(true);
                }

            }
        });

        return v;
    }

    public void llenarSpinner(){
        listaSpinner = BDhelper.spinnerTipoDoc();
        stringArrayList= new ArrayList<>();
        for (TipoDocumento a : listaSpinner){
            stringArrayList.add(a.getCatalogo());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    private void llenarTabla(){
        lista= BDhelper.listaDocumento(idTipoDocumento);
        stringArrayList= new ArrayList<>();
        if(!lista.isEmpty()){
            for (Documento a : lista){
                stringArrayList.add(a.getNombreDocumento()+"\n"+a.getNombreAutor()
                        +"\n"+a.getNombreEditorial()+" / "+a.getIsbm()
                        +"\n"+getString(R.string.listaCantidad)+" "+a.getCantidad());
            }
        }else{
            stringArrayList.add(getString(R.string.listEmpty));
        }

        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agregarDocumento:
                Documento d = new Documento();
                d.setIdDocente(idDocente);
                d.setIdDocumento(idDocumento);
                d.setNombreDocumento(nombreDoc);
                Snackbar.make(getView(), BDhelper.solicitarDocumento(d), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                break;
        }
    }
}
