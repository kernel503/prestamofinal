package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;
public class AutorEliminarActivity extends Activity {

    EditText editIdAutor;
    ControlBD controlhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autor_eliminar);
        controlhelper=new ControlBD (this);
        editIdAutor=(EditText)findViewById(R.id.editIdAutor);
    }
    public void eliminarAutor(View v){
        String regEliminadas;
        Autor autor=new Autor();
        autor.setIdAutor(Integer.parseInt(editIdAutor.getText().toString()));
        controlhelper.abrir();
        regEliminadas=controlhelper.eliminar(autor);
        controlhelper.cerrar();
        Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
    }
}
