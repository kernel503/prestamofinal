package edu.ues.fia.eisi.entrega1pdm.CM13106;

public class Documento {
    private static int idDocumento;
    private static int idAutor;
    private static int idTipoDoc;
    private String titulo;
    private String ISBM;
    private String idioma;
    private String detalle;
    private int cantidad;
    private String a1,a2,a3;

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public Documento(){
    }
    public Documento(int idDocumento, int idAutor, int idTipoDoc, String titulo, String ISBM, String idioma, String detalle, int cantidad) {
        this.idDocumento = idDocumento;
        this.idAutor = idAutor;
        this.idTipoDoc = idTipoDoc;
        this.titulo = titulo;
        this.ISBM = ISBM;
        this.idioma = idioma;
        this.detalle = detalle;
        this.cantidad = cantidad;
    }
    public int getIdDocumento() {
        return idDocumento;
    }


    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public static int getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }


    public static int getIdTipoDoc() {
        return idTipoDoc;
    }

    public void setIdTipoDoc(int idTipoDoc) {
        this.idTipoDoc = idTipoDoc;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getISBM() {
        return ISBM;
    }

    public void setISBM(String ISBM) {
        this.ISBM = ISBM;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
