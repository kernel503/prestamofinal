package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class TipoDocumentoActualizarActivity extends Activity {
    ControlBD helper;
    EditText editIdTipoDoc;
    EditText editCatalogo;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_actualizar);
        helper = new ControlBD(this);
        editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        editCatalogo = (EditText) findViewById(R.id.editCatalogo);
    }
    public void actualizarTipoDocumento(View v) {
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setIdTipoDoc(Integer.parseInt(editIdTipoDoc.getText().toString()));
        tipoDocumento.setCatalogo(editCatalogo.getText().toString());
        helper.abrir();
        String estado = helper.actualizar(tipoDocumento);
        helper.cerrar();
        Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
    }
    public void limpiarTexto(View v) {
        editIdTipoDoc.setText("");
        editCatalogo.setText("");

    }
}
