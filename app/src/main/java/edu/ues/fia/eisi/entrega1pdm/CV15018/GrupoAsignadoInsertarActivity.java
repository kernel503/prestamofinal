package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class GrupoAsignadoInsertarActivity extends AppCompatActivity {

    ControlBD helper;
    EditText et1,et2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupo_asignado_insertar);
        helper = new ControlBD(this);
        et1 = (EditText) findViewById(R.id.et_lugarAG);
        et2 = (EditText) findViewById(R.id.et_capacidadAG);

    }

    public void insertar(View v) {
        String lugar=et1.getText().toString();
        String capacidad=et2.getText().toString();

        String regInsertados;
        if(!lugar.isEmpty()&&!capacidad.isEmpty()) {
            GrupoAsignado asignacion = new GrupoAsignado();
            asignacion.setLugar(lugar);
            asignacion.setCapacidad(Integer.parseInt(capacidad));
            helper.abrir();
            regInsertados = helper.insertarAsignarGrupo(asignacion);
            helper.cerrar();
            Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Debe Conpletar todos los campos", Toast.LENGTH_SHORT).show();

        }

    }
}
