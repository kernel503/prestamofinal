package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import edu.ues.fia.eisi.entrega1pdm.R;

public class MainActivityCV15018 extends ListActivity {
    String[] menu={"Tabla Materia","Tabla Asignar Grupo","Tabla Equipo"};
    String[] activities={"MateriaMenuActivity","GrupoAsignadoMenuActivity","EquipoMenuActivity"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menu));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        String nombreValue=activities[position];

        try{
            Class<?> clase=Class.forName("edu.ues.fia.eisi.entrega1pdm.CV15018."+nombreValue);
            Intent inte = new Intent(this,clase);
            this.startActivity(inte);
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }


    }
}
