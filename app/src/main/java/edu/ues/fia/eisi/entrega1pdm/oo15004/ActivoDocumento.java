package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.R;

public class ActivoDocumento extends Fragment implements View.OnClickListener {
    View v;
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    FloatingActionButton btnDesactivar;
    ArrayList<Documento> lista = new ArrayList<>();
    int idPrestamoDocumento = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_activo_documento, container, false);
        BDhelper=new ControladorBD(getActivity());
        listView = v.findViewById(R.id.documentosActivos);
        btnDesactivar = v.findViewById(R.id.activoDoc);
        btnDesactivar.setOnClickListener(this);
        btnDesactivar.setEnabled(false);

        llenarTabla();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lista.isEmpty()){
                    idPrestamoDocumento = lista.get(index).getIdPrestamoDocumento();
                    btnDesactivar.setEnabled(true);
                }
            }
        });

        return v;
    }

    private void llenarTabla(){
        lista = BDhelper.listaActivoDocumento();
        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (Documento a : lista){
                stringArrayList.add(a.getNombreDocumento()+"\n"+a.getNombreAutor());
            }
        }else {
            stringArrayList.add(getString(R.string.listEmpty));
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activoDoc:
                Snackbar.make(getView(), BDhelper.devolverActivoDocumento(idPrestamoDocumento), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                btnDesactivar.setEnabled(false);
                break;
        }

    }
}
