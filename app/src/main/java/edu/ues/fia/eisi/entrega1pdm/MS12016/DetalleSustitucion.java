package edu.ues.fia.eisi.entrega1pdm.MS12016;

public class DetalleSustitucion {
    int idSustitucion;
    String motivo;
    String descripcion;

    public DetalleSustitucion() {
    }

    public DetalleSustitucion(int idSustitucion,String motivo, String descripcion) {
        this.idSustitucion = idSustitucion;
        this.motivo = motivo;
        this.descripcion = descripcion;

    }

    public int getIdSustitucion() {
        return idSustitucion;
    }

    public void setIdSustitucion(int idSustitucion) {
        this.idSustitucion = idSustitucion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
