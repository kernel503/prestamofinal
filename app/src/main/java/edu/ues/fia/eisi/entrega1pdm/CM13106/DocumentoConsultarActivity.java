package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DocumentoConsultarActivity extends Activity {

    ControlBD helper;
    EditText editIdDocumento;
    EditText editIdAutor;
    EditText editIdTipoDoc;
    EditText editTitulo;
    EditText editISBM;
    EditText editIdioma;
    EditText editDetalle;
    EditText editCantidad;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_consultar);
        helper = new ControlBD(this);
        editIdDocumento = (EditText) findViewById(R.id.editIdDocumento);
        editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        editTitulo = (EditText) findViewById(R.id.editTitulo);
        editISBM = (EditText) findViewById(R.id.editISBM);
        editIdioma = (EditText) findViewById(R.id.editIdioma);
        editDetalle = (EditText) findViewById(R.id.editDetalle);
        editCantidad = (EditText) findViewById(R.id.editCantidad);

    }
    public void consultarDocumento(View v) {
        helper.abrir();                           //son enteros ¿Como convertirlo a string?
        int IdDocumento = Integer.parseInt(editIdDocumento.getText().toString());
        //int idAutor = Integer.parseInt(editIdAutor.getText().toString());
        //int idTipoDoc= Integer.parseInt(editIdTipoDoc.getText().toString());
        Documento documento = helper.consultarDocumento(IdDocumento);
        helper.cerrar();
        if(documento == null)
            Toast.makeText(this, "Documento no registrada", Toast.LENGTH_LONG).show();
        else{
            editIdAutor.setText(String.valueOf(documento.getA1()));
            editIdTipoDoc.setText(String.valueOf(documento.getA2()));
            editDetalle.setText(String.valueOf(documento.getA3()));
            editTitulo.setText(String.valueOf(documento.getTitulo()));
            editISBM.setText(String.valueOf(documento.getISBM()));
            editIdioma.setText(String.valueOf(documento.getIdioma()));
            editCantidad.setText(String.valueOf(documento.getCantidad()));
        }
    }
    public void limpiarTexto(View v) {
        editIdDocumento.setText("");
        editIdAutor.setText("");
        editIdTipoDoc.setText("");
        editTitulo.setText("");
        editISBM.setText("");
        editIdioma.setText("");
        editDetalle.setText("");
        editCantidad.setText("");
    }
}
