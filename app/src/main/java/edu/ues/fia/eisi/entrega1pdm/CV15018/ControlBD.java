package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class ControlBD {

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "prestamoFinal.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                //db.execSQL("CREATE TABLE materia(idMateria INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT unique,codMateria VARCHAR(10),nombre VARCHAR(30),activo INTEGER NOT NULL)");
               // db.execSQL("CREATE TABLE GrupoAsignado(idGrupo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT unique,lugar text,capacidad INTEGER NOT NULL,activo INTEGER NOT NULL)");
               // db.execSQL("CREATE TABLE Equipo (idEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idUnidadAdmin INTEGER ,idClasificacion INTEGER, marca VARCHAR(40) NOT NULL, modelo VARCHAR(40) NOT NULL, cantidad INTEGER NOT NULL, fechaCompra DATE NOT NULL, activo INTEGER NOT NULL, FOREIGN KEY(idUnidadAdmin) REFERENCES UnidadAdmin(idUnidadAdmin));");
               // db.execSQL("CREATE TABLE UnidadAdmin (idUnidadAdmin INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, encargado VARCHAR(40)  NOT NULL UNIQUE, ubicacion VARCHAR(40)  NOT NULL UNIQUE, activo INTEGER NOT NULL);");
              //  db.execSQL("CREATE TABLE Clasificacion (idClasificacion INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, catalogo VARCHAR(40), activo INTEGER NOT NULL);");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }
    }

    public void abrir() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar() {
        DBHelper.close();
    }

    public String insertarMateria(Materia materia) {
        String regInsertados = "Registro Insertado Nº= ";
        long contador = 0;
        ContentValues mate = new ContentValues();
        mate.put("codMateria", materia.getCodMateria());
        mate.put("nombre", materia.getNombre());
        mate.put("activo",1);

        contador = db.insert("Materia", null, mate);


        if (contador == -1 || contador == 0) {
            regInsertados = "Error al Insertar el registro, Registro Duplicado. Verificar inserción";
        } else {
            regInsertados = regInsertados + contador;
        }
        return regInsertados;
    }


    public Materia consultarMateria(String id) {
        String[] pk = {id};
        final String[] camposMateria = new String[]{"idMateria", "codMateria", "nombre"};
        Cursor cursor = db.query("Materia", camposMateria, "idMateria = ? and activo = 1", pk, null, null, null);
        if (cursor.moveToFirst()) {
            Materia materia = new Materia();
            materia.setIdMateria(cursor.getInt(0));
            materia.setCodMateria(cursor.getString(1));
            materia.setNombre(cursor.getString(2));

            return materia;
        } else {
            return null;
        }
    }

    public String actualizarMateria(Materia materia) {
        //Falta INtegridad
        String[] id = {String.valueOf(materia.getIdMateria())};
        ContentValues registro = new ContentValues();
        registro.put("codMateria", materia.getCodMateria());
        registro.put("nombre", materia.getNombre());
        int i = db.update("Materia", registro, "idMateria = ?", id);
        if (i == 1)
            return "Registro Actualizado Correctamente";
        else
            return "ERROR Materia con " + materia.getIdMateria() + " No existe";

    }

    public String eliminarMateria(Materia materia) {
        String retorno = "";
        try {
            //db.execSQL("DELETE FROM UnidadAdmin WHERE idUnidadAdmin like "+u.getIdUnidadAdmin()+"");

            db.execSQL("UPDATE Materia SET activo = 0 WHERE idMateria = " + materia.getIdMateria() + "");
            retorno = "" + "Registro Eliminado" + " " + materia.getNombre() + " (" + materia.getCodMateria() + ")";
        } catch (SQLException ex) {
            retorno = "Error estableciendo Conexion  base de datos";
        }
        cerrar();
        return retorno;
    }

    //TABLA ASIGNARGRUPO
    public String insertarAsignarGrupo(GrupoAsignado asignacion) {
        String regInsertados = "Registro Insertado Nº= ";
        long contador = 0;
        ContentValues registro = new ContentValues();
        registro.put("lugar", asignacion.getLugar());
        registro.put("capacidad",asignacion.getCapacidad());
        registro.put("activo", 1);
        contador = db.insert("GrupoAsignado", null, registro);
        if (contador == -1 || contador == 0) {
            regInsertados = "Error al Insertar el registro, Registro Duplicado. Verificar inserción";
        } else {
            regInsertados = regInsertados + contador;
        }
        return regInsertados;
    }

    public GrupoAsignado consultarGrupoAsignado(String id) {
        String[] pk = {id};
        final String[] camposMateria = new String[]{"idGrupo","lugar","capacidad"};
        Cursor cursor = db.query("GrupoAsignado", camposMateria, "idGrupo = ? and activo = 1", pk, null, null, null);
        if (cursor.moveToFirst()) {
            GrupoAsignado asignado = new GrupoAsignado();
            asignado.setIdGrupo(cursor.getInt(0));
            asignado.setLugar(cursor.getString(1));
            asignado.setCapacidad(cursor.getInt(2));

            return asignado;
        } else {
            return null;
        }
    }

    public String actualizarGrupoAsignado(GrupoAsignado asignado) {
        //Falta INtegridad
        String[] id = {String.valueOf(asignado.getIdGrupo())};
        ContentValues registro = new ContentValues();
        registro.put("idGrupo", asignado.getIdGrupo());
        registro.put("lugar", asignado.getLugar());
        registro.put("capacidad", asignado.getCapacidad());
        int i = db.update("GrupoAsignado", registro, "idGrupo = ?", id);
        if (i == 1)
            return "Registro Actualizado Correctamente";
        else
            return "ERROR grupo con ID " + asignado.getIdGrupo() + " No existe";

    }

    public String eliminarGrupoAsignado(GrupoAsignado asignado){
        String retorno="";
        try {
            //db.execSQL("DELETE FROM UnidadAdmin WHERE idUnidadAdmin like "+u.getIdUnidadAdmin()+"");

            db.execSQL("UPDATE GrupoAsignado SET activo = 0 WHERE idGrupo = "+ asignado.getIdGrupo()+"");
            retorno=""+"Registro Eliminado" +" ID "+asignado.getIdGrupo();
        }catch (SQLException ex){
            retorno = "Error estableciendo Conexion  base de datos";
        }
        cerrar();
        return  retorno;
    }


    //TABLA EQUIPO

    public String insertarEquipo(Equipo e) {
        abrir();
        String retorno;
        try {
            String[] args = new String[7];
            args[0] = "" + e.getIdUnidadAdmin();
            args[1] = "" + e.getIdClasificacion();
            args[2] = "" + e.getMarca();
            args[3] = "" + e.getModelo();
            args[4] = "" + e.getCantidad();
            args[5] = "" + e.getFechaCompra();
            args[6] = "1";

            db.execSQL("INSERT INTO Equipo('idUnidadAdmin','idClasificacion','marca','modelo','cantidad','fechaCompra','activo') VALUES(?,?,?,?,?,?,?)", args);///!!!!!!!

            retorno = " " + "Registro Agregado" + " " + e.getMarca() + " (" + e.getModelo() + ")";
            ;
        } catch (SQLException ex) {
            retorno = "Error en el establecimiento de la conexion con la BD";
        }
        cerrar();
        return retorno;
    }

    public ArrayList listaUnidadAdmin() {//Metodo que accede a la base de datos y recupera los registros existentes de la tabla UnidadAdmin
        ArrayList<UnidadAdmin> lista = new ArrayList<>();
        abrir();
        Cursor c = db.rawQuery("SELECT * FROM UnidadAdmin WHERE activo = 1", null);


        if (c.moveToFirst()) {
            do {
                UnidadAdmin u = new UnidadAdmin();
                u.setIdUnidadAdmin(c.getInt(0));
                u.setEncargado(c.getString(1));
                u.setUbicacion(c.getString(2));
                lista.add(u);
            } while (c.moveToNext());
            {
            }
        }
        cerrar();
        return lista;
    }




    public Equipo consultarEquipo(String id) {
        String[] pk = {id};
        final String[] camposEquipo = new String[]{"idEquipo", "idUnidadAdmin","idClasificacion", "marca", "modelo", "cantidad", "fechaCompra"};
        Cursor cursor = db.query("Equipo", camposEquipo, "idEquipo = ? and activo = 1", pk, null, null, null);//Ojo seleccion
        if (cursor.moveToFirst()) {                                                                                                             //Debe incluir activo
            Equipo equipo = new Equipo();
            equipo.setId(cursor.getInt(0));
            equipo.setIdUnidadAdmin(cursor.getInt(1));
            equipo.setIdClasificacion(cursor.getInt(2));
            equipo.setMarca(cursor.getString(cursor.getColumnIndex("marca")));
            equipo.setModelo(cursor.getString(4));
            equipo.setCantidad(cursor.getInt(5));
            equipo.setFechaCompra(cursor.getString(cursor.getColumnIndex("fechaCompra")));


            return equipo;
        } else {
            return null;
        }
    }


    public String actualizarEquipo(Equipo equipo) {
        //Falta INtegridad
        String[] id = {String.valueOf(equipo.getId())};
        ContentValues registro = new ContentValues();
        registro.put("idUnidadAdmin", equipo.getIdUnidadAdmin());
        registro.put("idClasificacion", equipo.getIdClasificacion());
        registro.put("marca", equipo.getMarca());
        registro.put("modelo", equipo.getModelo());
        registro.put("cantidad", equipo.getCantidad());
        registro.put("fechaCompra", equipo.getFechaCompra());


        int i = db.update("Equipo", registro, "idEquipo = ?", id);
        if (i == 1)
            return "Registro Actualizado Correctamente";
        else
            return "ERROR Materia con " + equipo.getId() + " No existe";


    }

    public String eliminarEquipo(Equipo equipo){
        String retorno="";
        try {
            //db.execSQL("DELETE FROM UnidadAdmin WHERE idUnidadAdmin like "+u.getIdUnidadAdmin()+"");

            db.execSQL("UPDATE Equipo SET activo = 0 WHERE idEquipo = "+equipo.getId()+"");
            retorno=""+"Registro Eliminado" +" "+equipo.getMarca();
        }catch (SQLException ex){
            retorno = "Error estableciendo Conexion  base de datos";
        }
        cerrar();
        return  retorno;
    }

    public void llenarBD() {
        abrir();
        db.execSQL("DELETE FROM unidadAdmin");
        db.execSQL("DELETE FROM Clasificacion");

        final String[] nom = new String[]{"Daniel", "cortez", "Maria", "Roberto"};
        final String[] encargado = new String[]{"EISI", "EIA", "EII", "EIE"};

        final String[] catalogo = new String[]{"catalogo1", "catalogo2", "catalogo3", "catalogo4"};

        ContentValues registro = new ContentValues();
        ContentValues registroTClasi = new ContentValues();
        int i=0;
        int j=0;
        for (i = 0; i < 4; i++) {
            registro.put("encargado", nom[i]);
            registro.put("ubicacion", encargado[i]);
            registro.put("activo", 1);
            db.insert("UnidadAdmin", null, registro);
        }
        //llenando tabla Clasificacion

        for (j = 0; j < 4; j++) {
            registroTClasi.put("catalogo", catalogo[j]);
            registroTClasi.put("activo", 1);
            db.insert("Clasificacion", null, registroTClasi);
        }


        db.execSQL("DROP TABLE Materia");
        db.execSQL("CREATE TABLE materia(idMateria INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT unique,codMateria VARCHAR(10),nombre VARCHAR(30),activo INTEGER NOT NULL)");
        db.execSQL("DROP TABLE GrupoAsignado");
        db.execSQL("CREATE TABLE GrupoAsignado(idGrupo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT unique,lugar text,capacidad INTEGER NOT NULL,activo INTEGER NOT NULL)");

        db.execSQL("INSERT INTO materia('codMateria','nombre','activo') VALUES('MIP115','Microprogramacion',1)");
        db.execSQL("INSERT INTO materia('codMateria','nombre','activo') VALUES('DSI115','Diseño de Sistemas',1)");
        db.execSQL("INSERT INTO materia('codMateria','nombre','activo') VALUES('TAD115','Teoria Administrativa',1)");

        db.execSQL("INSERT INTO GrupoAsignado('lugar','capacidad','activo') VALUES('B43',100,1)");
        db.execSQL("INSERT INTO GrupoAsignado('lugar','capacidad','activo') VALUES('C42',90,1)");
        db.execSQL("INSERT INTO GrupoAsignado('lugar','capacidad','activo') VALUES('F1312',100,1)");

        cerrar();



    }

    public ArrayList listaClasificacion() {//Metodo que accede a la base de datos y recupera los registros existentes de la tabla Clasificacion
        ArrayList<Clasificacion> lista = new ArrayList<>();
        abrir();
        Cursor c = db.rawQuery("SELECT * FROM Clasificacion WHERE activo = 1", null);


        if (c.moveToFirst()) {
            do {
                Clasificacion clasi = new Clasificacion();
                clasi.setIdClasificacion(c.getInt(0));
                clasi.setCatalogo(c.getString(1));
                lista.add(clasi);
            } while (c.moveToNext());
            {
            }
        }
        cerrar();
        return lista;
    }
}

