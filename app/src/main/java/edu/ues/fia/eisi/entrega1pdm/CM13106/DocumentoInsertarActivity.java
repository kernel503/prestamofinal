package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DocumentoInsertarActivity extends Activity {
    ControlBD helper;
    EditText editIdDocumento;
    EditText editIdAutor;
    EditText editIdTipoDoc;
    EditText editTitulo;
    EditText editISBM;
    EditText editIdioma;
    EditText editDetalle;
    EditText editCantidad;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_documento_insertar);
        helper = new ControlBD(this);
        editIdDocumento = (EditText) findViewById(R.id.editIdDocumento);
        editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        editTitulo = (EditText) findViewById(R.id.editTitulo);
        editISBM = (EditText) findViewById(R.id.editISBM);
        editIdioma = (EditText) findViewById(R.id.editIdioma);
        editDetalle = (EditText) findViewById(R.id.editDetalle);
        editCantidad = (EditText) findViewById(R.id.editCantidad);
    }
    public void insertarDocumento(View v) {
        String idDocumento=editIdDocumento.getText().toString();
        String idAutor=editIdAutor.getText().toString();
        String idTipoDoc=editIdTipoDoc.getText().toString();
        String titulo=editTitulo.getText().toString();
        String ISBM=editISBM.getText().toString();
        String idioma=editIdioma.getText().toString();
        String detalle=editDetalle.getText().toString();
        String cantidad=editCantidad.getText().toString();

        String regInsertados;
        Documento documento=new Documento();
        documento.setIdDocumento(Integer.parseInt(idDocumento)); //entero

        documento.setIdTipoDoc(Integer.parseInt(idTipoDoc));
        documento.setIdAutor(Integer.parseInt(idAutor));
        documento.setDetalle(detalle);

        documento.setTitulo(titulo);
        documento.setISBM(ISBM);
        documento.setIdioma(idioma);
        documento.setCantidad(Integer.parseInt(cantidad));

        helper.abrir();
        regInsertados=helper.insertar(documento);
        helper.cerrar();
        Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
    }
    public void limpiarTexto(View v) {
        editIdDocumento.setText("");
        editIdAutor.setText("");
        editIdTipoDoc.setText("");
        editTitulo.setText("");
        editISBM.setText("");
        editIdioma.setText("");
        editDetalle.setText("");
        editCantidad.setText("");
    }
}
