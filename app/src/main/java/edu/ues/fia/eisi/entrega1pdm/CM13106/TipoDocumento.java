package edu.ues.fia.eisi.entrega1pdm.CM13106;

public class TipoDocumento {

    private int idTipoDoc;
    private  String catalogo;

    public TipoDocumento() {
    }

    public TipoDocumento(int idTipoDoc, String catalogo) {
        this.idTipoDoc = idTipoDoc;
        this.catalogo = catalogo;
    }

    public int getIdTipoDoc() {
        return idTipoDoc;
    }

    public void setIdTipoDoc(int idTipoDoc) {
        this.idTipoDoc = idTipoDoc;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }

}
