package edu.ues.fia.eisi.entrega1pdm.CV15018;

public class UnidadAdmin {
    private int idUnidadAdmin;
    private String encargado;
    private String ubicacion;


    public UnidadAdmin() {
    }


    public int getIdUnidadAdmin() {
        return idUnidadAdmin;
    }

    public void setIdUnidadAdmin(int idUnidadAdmin) {
        this.idUnidadAdmin = idUnidadAdmin;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
