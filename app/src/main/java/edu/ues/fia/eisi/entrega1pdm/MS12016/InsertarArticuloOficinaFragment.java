package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;



public class InsertarArticuloOficinaFragment extends Fragment implements View.OnClickListener{
    ControlBDMS12016 BDHelper;
    EditText editid;
    EditText editNombre;
    EditText editDistribuidor;
    EditText editCantidad;
    Button btnA_gregar;
    Button btnCancelar;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregar:
                String tost = "";
                if (editNombre.getText().toString().isEmpty() || editCantidad.getText().toString().isEmpty() || editDistribuidor.getText().toString().isEmpty()){
                    tost = "LOS CAMPOS ESTAN VACIOS!";
                }else{
                    ArticuloOficina articulo = new ArticuloOficina();
                    articulo.setNombre(editNombre.getText().toString());
                    articulo.setCantidad(Integer.parseInt(editCantidad.getText().toString()));
                    articulo.setDistribuidor(editDistribuidor.getText().toString());
                    BDHelper.abrir();
                    tost= BDHelper.insertarArticuloOficina(articulo);
                    BDHelper.cerrar();
                    limpiar();
                }
                Toast.makeText(getActivity(),tost,Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnCancelar:
                MostrarArticuloOficina fragment = new MostrarArticuloOficina();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;
        }

    }
    public void limpiar(){
        editNombre.setText("");
        editCantidad.toString();
        editDistribuidor.setText("");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        BDHelper =new ControlBDMS12016(getActivity());
        View v= inflater.inflate(R.layout.fragment_insertar_articulo_oficina, container, false);
        editNombre = (EditText) v.findViewById(R.id.editNombre);
        editCantidad = (EditText) v.findViewById(R.id.editCantidad);
        editDistribuidor= (EditText) v.findViewById(R.id.editDistribuidor);
        btnA_gregar = (Button)v.findViewById(R.id.btnAgregar);
        btnCancelar= (Button)v.findViewById(R.id.btnCancelar);
        btnA_gregar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        return v;
    }



}
