package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.ues.fia.eisi.entrega1pdm.Admin;
import edu.ues.fia.eisi.entrega1pdm.R;

public class ContenedorOO15004 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedor_oo15004);
    }
    public void horario(View v){
        Intent myIntent = new Intent(ContenedorOO15004.this, MostrarHorario.class);
        startActivity(myIntent);
    }
    public void admin(View v){
        Intent myIntent = new Intent(ContenedorOO15004.this, RegistroPrestamo.class);
        startActivity(myIntent);
    }

}
