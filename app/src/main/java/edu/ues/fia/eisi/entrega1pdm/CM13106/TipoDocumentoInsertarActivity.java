package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class TipoDocumentoInsertarActivity extends Activity {

    ControlBD helper;
    EditText editIdTipoDoc;
    EditText editCatalogo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_insertar);
        helper = new ControlBD(this);
        editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        editCatalogo = (EditText) findViewById(R.id.editCatalogo);
    }
    public void insertarTipoDocumento(View v) {
        String idTipoDoc=editIdTipoDoc.getText().toString();
        String catalogo=editCatalogo.getText().toString();
        String regInsertados; TipoDocumento tipoDocumento=new TipoDocumento();
        tipoDocumento.setIdTipoDoc(Integer.parseInt(idTipoDoc));
        tipoDocumento.setCatalogo(catalogo);
        helper.abrir();
        regInsertados=helper.insertar(tipoDocumento);
        helper.cerrar();
        Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
    }
    public void limpiarTexto(View v) {
        editIdTipoDoc.setText("");
        editCatalogo.setText("");
    }
}
