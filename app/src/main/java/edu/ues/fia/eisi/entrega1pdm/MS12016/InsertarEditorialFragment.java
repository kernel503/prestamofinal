package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;


public class InsertarEditorialFragment extends Fragment implements View.OnClickListener{
    ControlBDMS12016 BDHelper;
    EditText editEditorial;
    EditText Direccion;

    Button btnagregar;
    Button btncancelar;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnagregar:
                String tost = "";
                if (editEditorial.getText().toString().isEmpty() || Direccion.getText().toString().isEmpty()){
                    tost = "LOS CAMPOS ESTAN VACIOS!";
                }else{
                    Editorial editorial = new Editorial();
                    editorial.setNombre(editEditorial.getText().toString());
                    editorial.setDireccion(Direccion.getText().toString());
                    BDHelper.abrir();
                    tost= BDHelper.insertarEditorial(editorial);
                    BDHelper.cerrar();
                    limpiar();
                }
                Toast.makeText(getActivity(),tost,Toast.LENGTH_SHORT).show();
                break;
            case R.id.btncancelar:
                MostrarEditorial fragment = new MostrarEditorial();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;
        }

    }
    public void limpiar(){
        editEditorial.setText("");
        Direccion.setText("");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        BDHelper =new ControlBDMS12016(getActivity());
        View v= inflater.inflate(R.layout.fragment_insertar_editorial, container, false);
        editEditorial = (EditText) v.findViewById(R.id.editEditorial);
        Direccion = (EditText) v.findViewById(R.id.editDireccion);
        btnagregar = (Button)v.findViewById(R.id.btnagregar);
        btncancelar= (Button)v.findViewById(R.id.btncancelar);
        btnagregar.setOnClickListener(this);
        btncancelar.setOnClickListener(this);
        return v;
    }
}
