package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.ues.fia.eisi.entrega1pdm.R;


public class MostrarDetalleSustitucion extends Fragment implements View.OnClickListener {
    private ListView list_View;
    private Button btn_Editar;
    private Button btn_Eliminar;
    private ImageButton btn_Add;
    private List<String> stringArrayList ;
    ArrayList<DetalleSustitucion> lista = new ArrayList<>();
    private static final String Tag = "Mensajes";
    ControlBDMS12016 BDHelper;
    private Integer idSustitucion;
    private String motivo="";
    private String descripcion="";
    private ArrayAdapter<String> stringArrayAdapter;

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.btn_Add:
                InsertarDetalleSustitucionFragment fragment = new InsertarDetalleSustitucionFragment();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;

            case R.id.btn_Editar:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                Context context = getContext();
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final TextView ed = new TextView(context);
                ed.setText("EDITAR REGISTRO");
                layout.addView(ed);

                final TextView moti = new TextView(context);
                moti.setText("Motivo");
                layout.addView(moti);

                final EditText motiv = new EditText(context);
                motiv.setText(motivo);
                layout.addView(motiv);

                final TextView descr = new TextView(context);
                descr.setText("Descripcion");
                layout.addView(descr);

                final EditText descrip = new EditText(context);
                descrip.setText(descripcion);
                layout.addView(descrip);


                builder.setView(layout);

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tost = "";
                        motivo = motiv.getText().toString();
                        descripcion = descrip.getText().toString();
                        DetalleSustitucion detalle = new DetalleSustitucion(idSustitucion, motivo, descripcion);
                        BDHelper.abrir();
                        tost = BDHelper.actualizar(detalle);
                        Toast.makeText(getActivity(), tost, Toast.LENGTH_SHORT).show();
                        BDHelper.cerrar();
                        //  db.abrir();
                        //  db.Actualizar(articulo);
                        // db.cerrar();
                        llenarTabla();

                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

                break;

            case R.id.btn_Eliminar:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                builder2.setCancelable(true);
                builder2.setTitle("Confirmar eliminacion");
                builder2.setMessage("Eliminar registro: "+ motivo);
                builder2.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tosta = "";
                                BDHelper.abrir();
                                DetalleSustitucion ue = new DetalleSustitucion(idSustitucion, motivo, descripcion);
                                tosta=BDHelper.eliminar(ue);
                                BDHelper.cerrar();
                                Toast.makeText(getActivity(),tosta,Toast.LENGTH_SHORT).show();
                                llenarTabla();
                                btn_Editar.setEnabled(false);
                                btn_Eliminar.setEnabled(false);
                            }
                        });
                builder2.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder2.create();
                dialog.show();
                break;

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mostrar_detalle_sustitucion, container, false);
        BDHelper  = new ControlBDMS12016(getActivity());

        list_View=(ListView) v.findViewById(R.id.list_View);
        btn_Add =(ImageButton) v.findViewById(R.id.btn_Add);
        btn_Editar = (Button) v.findViewById(R.id.btn_Editar);
        btn_Eliminar = (Button) v.findViewById(R.id.btn_Eliminar);

        btn_Eliminar.setOnClickListener(this);
        btn_Editar.setOnClickListener(this);
        btn_Add.setOnClickListener(this);
        btn_Editar.setEnabled(false);
        btn_Eliminar.setEnabled(false);

        llenarTabla();

        list_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                btn_Editar.setEnabled(true);
                btn_Eliminar.setEnabled(true);

                idSustitucion=lista.get(index).getIdSustitucion();
                motivo= lista.get(index).getMotivo();
                descripcion= lista.get(index).getDescripcion();

            }
        });

        return v;
    }


    private void llenarTabla(){
        lista= BDHelper.listaDetalleSustitucion();
        stringArrayList= new ArrayList<>();
        for (DetalleSustitucion a : lista){
            stringArrayList.add("motivo: "+a.getMotivo()+" \nDescripcion: "+a.getDescripcion());
        }

        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        list_View.setAdapter(stringArrayAdapter);
    }



}
