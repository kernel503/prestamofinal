package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class TipoDocumentoEliminarActivity extends Activity {

    EditText editIdTipoDoc;
    ControlBD controlhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_documento_eliminar);
        controlhelper=new ControlBD (this);
        editIdTipoDoc=(EditText)findViewById(R.id.editIdTipoDoc);
    }
    public void eliminarTipoDocumento(View v){
        String regEliminadas;
        TipoDocumento tipoDocumento=new TipoDocumento();
        tipoDocumento.setIdTipoDoc(Integer.parseInt(editIdTipoDoc.getText().toString()));
        controlhelper.abrir();
        regEliminadas=controlhelper.eliminar(tipoDocumento);
        controlhelper.cerrar();
        Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
    }
}
