package edu.ues.fia.eisi.entrega1pdm;

public class Docente {
    public static String getNombreDocente() {
        return nombreDocente;
    }

    public static void setNombreDocente(String nombreDocente) {
        Docente.nombreDocente = nombreDocente;
    }

    public static int getIdDocente() {
        return idDocente;
    }

    public static void setIdDocente(int idDocente) {
        Docente.idDocente = idDocente;
    }

    public Docente() {
    }
    static String nombreDocente="Carlos Orellana";
    static int idDocente = 2;
    static int acceso;

    public int getAcceso() {
        return acceso;
    }

    public void setAcceso(int acceso) {
        this.acceso = acceso;
    }
}
