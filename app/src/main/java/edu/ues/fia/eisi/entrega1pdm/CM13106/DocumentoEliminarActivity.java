package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DocumentoEliminarActivity extends Activity {

    EditText editIdDocumento,editIdAutor,editIdTipoDoc;
    ControlBD controlhelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_eliminar);
        controlhelper=new ControlBD(this);
        editIdDocumento=(EditText)findViewById(R.id.editIdDocumento);
        editIdAutor=(EditText)findViewById(R.id.editIdAutor);
        editIdTipoDoc=(EditText)findViewById(R.id.editIdTipoDoc);
    }
    public void eliminarDocumento(View v){
        String regEliminadas;
        Documento documento=new Documento();
        documento.setIdDocumento(Integer.parseInt(editIdDocumento.getText().toString()));
        //documento.setIdAutor(Integer.parseInt(editIdAutor.getText().toString()));
        //documento.setIdTipoDoc(Integer.parseInt(editIdTipoDoc.getText().toString()));
        controlhelper.abrir();
        regEliminadas=controlhelper.eliminar(documento);
        controlhelper.cerrar();
        Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
    }
    public void limpiar(View v){
        editIdDocumento.setText("");
        editIdAutor.setText("");
        editIdTipoDoc.setText("");
    }
}
