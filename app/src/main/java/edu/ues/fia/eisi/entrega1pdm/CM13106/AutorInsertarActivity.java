package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class AutorInsertarActivity extends Activity {

    ControlBD helper;
    EditText editIdAutor;
    EditText editNombre;
    EditText editNacionalidad;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_autor_insertar);
        helper = new ControlBD(this);
        editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        editNombre = (EditText) findViewById(R.id.editNombre);
        editNacionalidad = (EditText) findViewById(R.id.editNacionalidad);
    }
    public void insertarAutor(View v) {
        String idAutor=editIdAutor.getText().toString();
        String nombre=editNombre.getText().toString();
        String nacionalidad=editNacionalidad.getText().toString();
        String regInsertados;
        Autor autor=new Autor();
        autor.setIdAutor(Integer.parseInt(idAutor));
        autor.setNombre(nombre);
        autor.setNacionalidad(nacionalidad);
        helper.abrir();
        regInsertados=helper.insertar(autor);
        helper.cerrar();
        Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
    }
    public void limpiarTexto(View v) {
        editIdAutor.setText("");
        editNombre.setText("");
        editNacionalidad.setText("");
    }

}
