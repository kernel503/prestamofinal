package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.R;

public class MostrarHorario extends AppCompatActivity {
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    ArrayList<Horario> lista = new ArrayList<>();
    int idHorario;
    String dia, inicio,fin;
    FloatingActionMenu btnMenu;
    FloatingActionButton btnEdit,btnDelete ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_horario);
        BDhelper=new ControladorBD(this);
        listView = findViewById(R.id.listaHorario);

        btnMenu = (FloatingActionMenu) findViewById(R.id.menuHorario);
        btnEdit = (FloatingActionButton)findViewById(R.id.editHorario);
        btnDelete = (FloatingActionButton)findViewById(R.id.deleteHorario);

        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);

        llenarTabla();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                idHorario = lista.get(index).getIdHorario();
                dia = lista.get(index).getDia();
                inicio = lista.get(index).getInicio();
                fin = lista.get(index).getFin();
                btnEdit.setEnabled(true);
                btnDelete.setEnabled(true);
            }
        });

    }
    private void llenarTabla(){
        lista= BDhelper.listaHorario();
        stringArrayList= new ArrayList<>();
        for (Horario a : lista){
            stringArrayList.add(a.getDia()+": "+a.getInicio()+" - "+a.getFin());
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    public void agregarHorario(View v){
        btnMenu.close(true);
        int opc = 0;
        Intent i = new Intent(getBaseContext(), GestionarHorario.class);
        i.putExtra("opc", opc);
        startActivity(i);
    }
    public void editarHorario(View v){
        btnMenu.close(true);
        int opc = 1;
        Intent i = new Intent(getBaseContext(), GestionarHorario.class);
        i.putExtra("opc", opc);
        i.putExtra("id",idHorario);
        i.putExtra("dia",dia);
        i.putExtra("inicio",inicio);
        i.putExtra("fin",fin);
        startActivity(i);

    }
    public void eliminarHorario(View v){
        btnMenu.close(true);
        BDhelper.eliminarHorario(idHorario);
        llenarTabla();
    }

}
