package edu.ues.fia.eisi.entrega1pdm;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import edu.ues.fia.eisi.entrega1pdm.oo15004.Documento;
import edu.ues.fia.eisi.entrega1pdm.oo15004.Equipo;

public class ControladorBDPrincipal {

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControladorBDPrincipal(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "prestamoFinal.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
                db.execSQL("CREATE TABLE ArticuloOficina (idArticulo INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, distribuidor TEXT NOT NULL, cantidad INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE Autor (idAutor INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, nacionalidad TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Clasificacion (idClasificacion INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL, activo INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE DetalleSustitucion (idSustitucion INTEGER PRIMARY KEY AUTOINCREMENT, motivo TEXT NOT NULL, descripcion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Docente (idDocente INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, cargo TEXT NOT NULL, fechaContratacion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Documento (idDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idTipoDocumento INTEGER NOT NULL, idAutor INTEGER NOT NULL, idEditorial INTEGER NOT NULL, titulo TEXT NOT NULL, isbm TEXT NOT NULL, idioma TEXT NOT NULL, cantidad INTEGER NOT NULL, FOREIGN KEY(idEditorial) REFERENCES Editorial, FOREIGN KEY(idTipoDocumento) REFERENCES TipoDocumento, FOREIGN KEY(idAutor) REFERENCES Autor);");
                db.execSQL("CREATE TABLE Editorial (idEditorial INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL,direccion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Equipo (idEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idClasificacion INTEGER NOT NULL, idUnidadAdmin	INTEGER NOT NULL, marca TEXT NOT NULL, modelo TEXT NOT NULL, cantidad INTEGER NOT NULL, fechaCompra TEXT NOT NULL, activo INTEGER NOT NULL, FOREIGN KEY(idClasificacion) REFERENCES Clasificacion, FOREIGN KEY(idUnidadAdmin) REFERENCES UnidadAdmin);");
                db.execSQL("CREATE TABLE GrupoAsignado (idGrupo INTEGER PRIMARY KEY AUTOINCREMENT, lugar TEXT NOT NULL, capacidad INTEGER NOT NULL,activo INTEGER NOT NULL);");//ACTIVO AGREGADO
                db.execSQL("CREATE TABLE Horario (idHorario INTEGER PRIMARY KEY AUTOINCREMENT, dia TEXT NOT NULL, inicio TEXT NOT NULL, fin TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Inventario (idInventario INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER, idDocumento INTEGER,idArticulo INTEGER,cantidadInicial INTEGER NOT NULL,cantidadFinal INTEGER NOT NULL, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina);");
                db.execSQL("CREATE TABLE Materia (idMateria INTEGER PRIMARY KEY AUTOINCREMENT, codMateria TEXT NOT NULL, nombre TEXT NOT NULL,activo INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE TipoDocumento (idTipoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL);");
                db.execSQL("CREATE TABLE UnidadAdmin (idUnidadAdmin INTEGER PRIMARY KEY AUTOINCREMENT, escuela TEXT NOT NULL, unidad TEXT NOT NULL, encargado TEXT NOT NULL, activo INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE PrestamoArticulo (idPrestamoArticulo INTEGER PRIMARY KEY AUTOINCREMENT, idArticulo INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                db.execSQL("CREATE TABLE PrestamoDocumento (idPrestamoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idDocumento INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                db.execSQL("CREATE TABLE PrestamoEquipo (idPrestamoEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER NOT NULL, idGrupo INTEGER NOT NULL, idDocente INTEGER NOT NULL, idMateria INTEGER NOT NULL, activo INTEGER NOT NULL, espacio1 INTEGER, espacio2 INTEGER, espacio3 INTEGER, idSustitucion INTEGER, FOREIGN KEY(idMateria) REFERENCES Materia, FOREIGN KEY(idGrupo) REFERENCES GrupoAsignado, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                db.execSQL("CREATE TABLE AccesoUsuario (idAceso INTEGER PRIMARY KEY AUTOINCREMENT,idOpcion INTEGER NOT NULL,idUsuario INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE OpcionCrud (idOpcion INTEGER PRIMARY KEY AUTOINCREMENT,descOpcion TEXT NOT NULL,numCrud INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE Usuario (idUsuario INTEGER PRIMARY KEY AUTOINCREMENT,NomUsuario TEXT NOT NULL,Clave TEXT NOT NULL,idDocente INTEGER NOT NULL,FOREIGN KEY(idDocente) REFERENCES Docente);");
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        }

    }

    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar(){
        DBHelper.close();
    }

    public String CrearTriggers(){
        String valor ="";
        abrir();
        try {
            db.execSQL("CREATE TRIGGER IF NOT EXISTS agregarEquipo AFTER INSERT ON Equipo BEGIN INSERT INTO Inventario(idEquipo, cantidadInicial, cantidadFinal) VALUES (new.idEquipo, new.cantidad, new.cantidad); END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS agregarDocumento AFTER INSERT ON Documento BEGIN INSERT INTO Inventario(idDocumento, cantidadInicial, cantidadFinal) VALUES (new.idDocumento, new.cantidad, new.cantidad); END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS agregarArticulo AFTER INSERT ON ArticuloOficina BEGIN INSERT INTO Inventario(idArticulo, cantidadInicial, cantidadFinal) VALUES (new.idArticulo, new.cantidad, new.cantidad); END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS aumDocumento AFTER UPDATE OF activo ON PrestamoDocumento FOR EACH ROW WHEN new.activo == 0 BEGIN UPDATE Documento SET cantidad = cantidad+1 WHERE idDocumento=new.idDocumento; END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS decDocumento AFTER INSERT ON PrestamoDocumento BEGIN UPDATE Documento SET cantidad = cantidad-1 WHERE idDocumento=new.idDocumento; END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS aumArticulo AFTER UPDATE OF activo ON PrestamoArticulo FOR EACH ROW WHEN new.activo == 0 BEGIN UPDATE ArticuloOficina SET cantidad = cantidad+1 WHERE idArticulo=new.idArticulo; END;");
            db.execSQL("CREATE TRIGGER IF NOT EXISTS decArticulo AFTER INSERT ON PrestamoArticulo BEGIN UPDATE ArticuloOficina SET cantidad = cantidad-1 WHERE idArticulo=new.idArticulo; END;");
            valor = "Triggers Creado";
        }catch (SQLException ex){
            valor = "Error Triggers";
            Log.i(Tag, "Revisar sintaxis ");
        }
        cerrar();
        return valor;
    }

    public String llenarBase(){
        String valor ="";
        abrir();
        try {
            db.execSQL("DROP TABLE AccesoUsuario;");
            db.execSQL("DROP TABLE ArticuloOficina;");
            db.execSQL("DROP TABLE Autor;");
            db.execSQL("DROP TABLE Clasificacion;");
            db.execSQL("DROP TABLE DetalleSustitucion;");
            db.execSQL("DROP TABLE Docente;");
            db.execSQL("DROP TABLE Documento;");
            db.execSQL("DROP TABLE Editorial;");
            db.execSQL("DROP TABLE Equipo;");
            db.execSQL("DROP TABLE GrupoAsignado;");
            db.execSQL("DROP TABLE Horario;");
            db.execSQL("DROP TABLE Inventario;");
            db.execSQL("DROP TABLE Materia;");
            db.execSQL("DROP TABLE TipoDocumento;");
            db.execSQL("DROP TABLE PrestamoDocumento;");
            db.execSQL("DROP TABLE PrestamoEquipo;");
            db.execSQL("DROP TABLE PrestamoArticulo;");
            db.execSQL("DROP TABLE OpcionCrud;");
            db.execSQL("DROP TABLE UnidadAdmin;");
            db.execSQL("DROP TABLE Usuario;");

            db.execSQL("CREATE TABLE ArticuloOficina (idArticulo INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, distribuidor TEXT NOT NULL, cantidad INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE Autor (idAutor INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, nacionalidad TEXT NOT NULL);");
            db.execSQL("CREATE TABLE Clasificacion (idClasificacion INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL, activo INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE DetalleSustitucion (idSustitucion INTEGER PRIMARY KEY AUTOINCREMENT, motivo TEXT NOT NULL, descripcion TEXT NOT NULL);");
            db.execSQL("CREATE TABLE Docente (idDocente INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, cargo TEXT NOT NULL, fechaContratacion TEXT NOT NULL);");
            db.execSQL("CREATE TABLE Documento (idDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idTipoDocumento INTEGER NOT NULL, idAutor INTEGER NOT NULL, idEditorial INTEGER NOT NULL, titulo TEXT NOT NULL, isbm TEXT NOT NULL, idioma TEXT NOT NULL, cantidad INTEGER NOT NULL, FOREIGN KEY(idEditorial) REFERENCES Editorial, FOREIGN KEY(idTipoDocumento) REFERENCES TipoDocumento, FOREIGN KEY(idAutor) REFERENCES Autor);");
            db.execSQL("CREATE TABLE Editorial (idEditorial INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL,direccion TEXT NOT NULL);");
            db.execSQL("CREATE TABLE Equipo (idEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idClasificacion INTEGER NOT NULL, idUnidadAdmin	INTEGER NOT NULL, marca TEXT NOT NULL, modelo TEXT NOT NULL, cantidad INTEGER NOT NULL, fechaCompra TEXT NOT NULL, activo INTEGER NOT NULL, FOREIGN KEY(idClasificacion) REFERENCES Clasificacion, FOREIGN KEY(idUnidadAdmin) REFERENCES UnidadAdmin);");
            db.execSQL("CREATE TABLE GrupoAsignado (idGrupo INTEGER PRIMARY KEY AUTOINCREMENT, lugar TEXT NOT NULL, capacidad INTEGER NOT NULL,activo INTEGER NOT NULL);");//Activo agregado
            db.execSQL("CREATE TABLE Horario (idHorario INTEGER PRIMARY KEY AUTOINCREMENT, dia TEXT NOT NULL, inicio TEXT NOT NULL, fin TEXT NOT NULL);");
            db.execSQL("CREATE TABLE Inventario (idInventario INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER, idDocumento INTEGER,idArticulo INTEGER,cantidadInicial INTEGER NOT NULL,cantidadFinal INTEGER NOT NULL, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina);");
            db.execSQL("CREATE TABLE Materia (idMateria INTEGER PRIMARY KEY AUTOINCREMENT, codMateria TEXT NOT NULL, nombre TEXT NOT NULL,activo INTEGER NOT NULL);");//ATRIBUTO ACTIVO
            db.execSQL("CREATE TABLE TipoDocumento (idTipoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL);");
            db.execSQL("CREATE TABLE UnidadAdmin (idUnidadAdmin INTEGER PRIMARY KEY AUTOINCREMENT, escuela TEXT NOT NULL, unidad TEXT NOT NULL, encargado TEXT NOT NULL, activo INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE PrestamoArticulo (idPrestamoArticulo INTEGER PRIMARY KEY AUTOINCREMENT, idArticulo INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
            db.execSQL("CREATE TABLE PrestamoDocumento (idPrestamoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idDocumento INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
            db.execSQL("CREATE TABLE PrestamoEquipo (idPrestamoEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER NOT NULL, idGrupo INTEGER NOT NULL, idDocente INTEGER NOT NULL, idMateria INTEGER NOT NULL, activo INTEGER NOT NULL, espacio1 INTEGER, espacio2 INTEGER, espacio3 INTEGER, idSustitucion INTEGER, FOREIGN KEY(idMateria) REFERENCES Materia, FOREIGN KEY(idGrupo) REFERENCES GrupoAsignado, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
            db.execSQL("CREATE TABLE AccesoUsuario (idAceso INTEGER PRIMARY KEY AUTOINCREMENT,idOpcion INTEGER NOT NULL,idUsuario INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE OpcionCrud (idOpcion INTEGER PRIMARY KEY AUTOINCREMENT,descOpcion TEXT NOT NULL,numCrud INTEGER NOT NULL);");
            db.execSQL("CREATE TABLE Usuario (idUsuario INTEGER PRIMARY KEY AUTOINCREMENT,NomUsuario TEXT NOT NULL,Clave TEXT NOT NULL,idDocente INTEGER NOT NULL,FOREIGN KEY(idDocente) REFERENCES Docente);");

            db.execSQL("INSERT INTO ArticuloOficina (nombre, distribuidor, cantidad) VALUES ('Plumones (Azules)','Librería Universitaria', 4);");
            db.execSQL("INSERT INTO ArticuloOficina (nombre, distribuidor, cantidad) VALUES ('Borrador','Librería Universitaria', 4);");
            //ADD
            db.execSQL("INSERT INTO ArticuloOficina (nombre, distribuidor, cantidad) VALUES ('Grapadora','Librería Universitaria', 2);");

            db.execSQL("INSERT INTO Autor (nombre, nacionalidad) VALUES ('Gabriela Azucena Campos García','Salvadoreña');");
            db.execSQL("INSERT INTO Autor (nombre, nacionalidad) VALUES ('Roberto Martínez Román','Colombiano');");
            db.execSQL("INSERT INTO Autor (nombre, nacionalidad) VALUES ('William Stallings','Estadounidense');");
            db.execSQL("INSERT INTO Autor (nombre, nacionalidad) VALUES ('Cesar Ernesto Martinez Gochez','Salvadoreño');");

            db.execSQL("INSERT INTO Clasificacion (catalogo, activo) VALUES ('Proyector',1);");
            db.execSQL("INSERT INTO Clasificacion (catalogo, activo) VALUES ('Switch',1);");
            db.execSQL("INSERT INTO Clasificacion (catalogo, activo) VALUES ('Laptop',1);");

            db.execSQL("INSERT INTO DetalleSustitucion (motivo, descripcion) VALUES ('Desaparecido','');");
            db.execSQL("INSERT INTO DetalleSustitucion (motivo, descripcion) VALUES ('Mantenimiento','');");
            db.execSQL("INSERT INTO DetalleSustitucion (motivo, descripcion) VALUES ('Arruinado','');");

            db.execSQL("INSERT INTO Documento (idTipoDocumento, idAutor, idEditorial, titulo, isbm, idioma, cantidad) VALUES (1,2, 1, 'Cómo Programar en Java. Séptima edición', '978-970-26-1190-5', 'Español', 4);");
            db.execSQL("INSERT INTO Documento (idTipoDocumento, idAutor, idEditorial, titulo, isbm, idioma, cantidad) VALUES (1,3, 2, 'Organización y Arquitectura de Computadores', '9788489660243', 'Español', 2);");
            db.execSQL("INSERT INTO Documento (idTipoDocumento, idAutor, idEditorial, titulo, isbm, idioma, cantidad) VALUES (2,4, 2, 'Protección Integral de la Niñez y Adolescencia (LEPINA)', '-', 'Español', 1);");

            db.execSQL("INSERT INTO Editorial (nombre, direccion) VALUES ('Prentice Hall','Pearson Educación de México, S.A. de C.V.');");
            db.execSQL("INSERT INTO Editorial (nombre, direccion) VALUES ('McGraw-Hill','Nueva York, Estados Unidos');");

            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (1,1, 'Panasonic', 'PT-LB280E', 1, '2017-8-5', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (1,2, 'BenQ', 'MW705', 1, '2019-2-2-', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (3,2, 'Dell', 'XPS 13', 1, '2019-3-4', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (3,2, 'HP', 'Envy 13', 1, '2018-9-10', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (2,2, 'Cisco', 'C3750X', 1, '2019-2-6', 1);");
            //ADD
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (3,3, 'Dell', 'XPS 13', 1, '2019-3-4', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (3,3, 'HP', 'Envy 13', 1, '2018-9-10', 1);");
            db.execSQL("INSERT INTO Equipo (idClasificacion, idUnidadAdmin, marca, modelo, cantidad, fechaCompra, activo) VALUES (2,4, 'Cisco', 'C3750X', 1, '2019-2-6', 1);");

            db.execSQL("INSERT INTO GrupoAsignado (lugar, capacidad,activo) VALUES ('Salón Espino',100,1);");//ACTIVO
            db.execSQL("INSERT INTO GrupoAsignado (lugar, capacidad,activo) VALUES ('Salón D1',98,1);");
            db.execSQL("INSERT INTO GrupoAsignado (lugar, capacidad,activo) VALUES ('Salón C11',96,1);");

            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Lunes','6:35 p. m.', '8:05 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Martes','6:35 p. m.', '8:05 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Miércoles','6:35 p. m.', '8:05 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Jueves','6:35 p. m.', '8:05 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Viernes','6:35 p. m.', '8:05 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','6:30 a. m.', '8:00 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','8:05 a. m.', '9:45 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','9:50 a. m.', '11:30 a. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','11:35 a. m.', '1:15 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','1:20 p. m.', '3:00 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','3:05 p. m.', '4:45 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','4:50 p. m.', '6:30 p. m.');");
            db.execSQL("INSERT INTO Horario (dia, inicio, fin) VALUES ('Sábado','6:35 p. m.', '8:15 p. m.');");

            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (NULL, 1, NULL, 4, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (NULL, 2, NULL, 2, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (NULL, 3, NULL, 1, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (1, NULL, NULL, 4, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (2, NULL, NULL, 1, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (3, NULL, NULL, 1, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (4, NULL, NULL, 1, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (NULL, NULL, 1, 4, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (NULL, NULL, 2, 4, 0);");
            db.execSQL("INSERT INTO Inventario (idEquipo, idDocumento, idArticulo, cantidadInicial, cantidadFinal) VALUES (5, NULL, NULL, 5, 0);");

            db.execSQL("INSERT INTO Materia (codMateria, nombre,activo) VALUES ('SDU','Sistemas Digitales',1);");
            db.execSQL("INSERT INTO Materia (codMateria, nombre,activo) VALUES ('MAT1','Matemática 1',1);");
            db.execSQL("INSERT INTO Materia (codMateria, nombre,activo) VALUES ('PRN3','Programación 3',1);");
            db.execSQL("INSERT INTO Materia (codMateria, nombre,activo) VALUES ('TSI115','Teoría de Sistemas',1);");

            db.execSQL("INSERT INTO TipoDocumento (catalogo) VALUES ('Libro');");
            db.execSQL("INSERT INTO TipoDocumento (catalogo) VALUES ('Tesis');");
            db.execSQL("INSERT INTO TipoDocumento (catalogo) VALUES ('Reporte');");

            db.execSQL("INSERT INTO UnidadAdmin (escuela, unidad, encargado, activo) VALUES ('Escuela de Ingeniería de Sistemas Informáticos','Cubículos','Nelson Zarate Sanchez', 1);");
            db.execSQL("INSERT INTO UnidadAdmin (escuela, unidad, encargado, activo) VALUES ('Escuela de Ingeniería de Sistemas Informáticos','Bodega','Jose Modesto Ventura', 1);");
            //ADD
            db.execSQL("INSERT INTO UnidadAdmin (escuela, unidad, encargado, activo) VALUES ('Escuela de Ingeniería Industrial','Cubículos','David Moncada', 1);");
            db.execSQL("INSERT INTO UnidadAdmin (escuela, unidad, encargado, activo) VALUES ('Escuela de Ingeniería Mecánica','Cubículos','Paulina Londoño', 1);");

            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Juan Carlos Orellana','Administrador', '2010-8-2');");
            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Juan Carlos Orellana Orellana','Administrador', '2010-8-2');");
            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Iveth Edelmira Carranza Martinez','Docente', '2012-05-20');");
            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Francisco Daniel Cortez Vasquez','Docente', '2013-9-6');");
            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Delmy Stefany Menjivar Sandoval','Docente', '2005-3-6');");
            db.execSQL("INSERT INTO Docente (nombre, cargo, fechaContratacion) VALUES ('Juan Carlos Orellana Orellana','Encargado', '2010-8-2');");

            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('admin','admin', 1);");
            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('oo15004','oo15004', 2);");
            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('cm13106','cm13106', 3);");
            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('cv15018','cv15018', 4);");
            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('ms12016','ms12016', 5);");
            db.execSQL("INSERT INTO Usuario (nomUsuario, clave, idDocente) VALUES ('OO15004','OO15004', 6);");

            db.execSQL("INSERT INTO OpcionCrud (descOpcion, numCrud) VALUES ('administrador',0);");
            db.execSQL("INSERT INTO OpcionCrud (descOpcion, numCrud) VALUES ('docente',0);");
            db.execSQL("INSERT INTO OpcionCrud (descOpcion, numCrud) VALUES ('encargado',0);");

            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (1,1);");
            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (2,2);");
            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (2,3);");
            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (1,4);");
            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (2,5);");
            db.execSQL("INSERT INTO AccesoUsuario (idOpcion, idUsuario) VALUES (3,6);");

            valor = "Datos Iniciales";
        }catch (SQLException ex){
            valor = "Error Datos iniciales";
            Log.i(Tag, "Revisar sintaxis ");
        }
        cerrar();
        return valor;
    }

    public boolean usuario(String editUser){
        abrir();
        boolean u = false;
        String[] args = new String[1];
        args[0]=""+editUser;
        try{
            Cursor c= db.rawQuery("SELECT * FROM Usuario AS u " +
                    "WHERE NomUsuario = ?",args);
            if(c.moveToFirst()){
                u = true;
            }
        }catch (SQLException ex){ }
        cerrar();
        return u;
    }
    public boolean contra(String editUser, String editPass){
        abrir();
        boolean u = false;
        String[] args = new String[2];
        args[0]=""+editUser;
        args[1]=""+editPass;
        try{
            Cursor c= db.rawQuery("SELECT * FROM Usuario AS u " +
                    "WHERE NomUsuario = ? AND Clave =?",args);
            if(c.moveToFirst()){
                u = true;
            }
        }catch (SQLException ex){ }
        cerrar();
        return u;
    }

    public void datos(String editUser, String editPass){
        abrir();
        Docente d = new Docente();
        String[] args = new String[2];
        args[0]=""+editUser;
        args[1]=""+editPass;
        try{
            Cursor c= db.rawQuery("SELECT a.idOpcion, d.idDocente ,d.nombre " +
                    "FROM AccesoUsuario as A " +
                    "INNER JOIN Usuario AS u USING (idUsuario) " +
                    "INNER JOIN Docente AS d USING (idDocente) " +
                    "INNER JOIN OpcionCrud USING (idOpcion) " +
                    "WHERE u.NomUsuario = ? AND u.Clave = ?",args);
            if(c.moveToFirst()){
                    d.setAcceso(c.getInt(0));
                    d.setIdDocente(c.getInt(1));
                    d.setNombreDocente(c.getString(2));
            }
        }catch (SQLException ex){ }
        cerrar();
    }
}
