package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.R;

public class EquipoInsertarActivity extends AppCompatActivity {

    private static final String Tag = "Mensajes";
    //Arrays para el llenado del Spinner UnidadAdmin
    ArrayList<String> listToSpinner;
    ArrayList<UnidadAdmin> listaUnidades = new ArrayList<>();

    //Arrays para el llenado del Spinner Clasificacion  paso1
    ArrayList<String> listToSpinnerClasif;
    ArrayList<Clasificacion> listaClasificaciones = new ArrayList<>();

    Spinner spinner;
    Spinner spinnerC;

    Button btnCancelar, btnOpc;
    EditText editMarca, editModelo, editCantidad, editFecha;
    int idUnidadAdmin;
    int idClasificacion;
    String index;
    ControlBD BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_insertar);

        BDhelper=new ControlBD(this);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerC = (Spinner) findViewById(R.id.spinnerClasif);
        editMarca = (EditText) findViewById(R.id.editMarca);
        editModelo = (EditText)findViewById(R.id.editModelo);
        editCantidad = (EditText)findViewById(R.id.editCantidad);
        editFecha = (EditText)findViewById(R.id.editFecha);
        llenarSpinner();

        //obtener valor/es a usar cuando se selecciona una opccion del spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {
                    idUnidadAdmin = listaUnidades.get(position-1).getIdUnidadAdmin();
                    Toast.makeText(parent.getContext(),"id"+idUnidadAdmin ,Toast.LENGTH_SHORT).show();
                }else{
                    idUnidadAdmin=0;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //obtener valor/es a usar cuando se selecciona una opccion del spinner2 PASO2
        spinnerC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {
                    idClasificacion = listaClasificaciones.get(position-1).getIdClasificacion();
                    Toast.makeText(parent.getContext(),"id"+idClasificacion ,Toast.LENGTH_SHORT).show();
                }else{
                    idClasificacion=0;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });






    }

    private void llenarSpinner() {
        listaUnidades = BDhelper.listaUnidadAdmin();
        listToSpinner = new ArrayList<>();
        listToSpinner.add("SELECCIONE");
        for(UnidadAdmin a : listaUnidades){
            listToSpinner.add(a.getUbicacion()+" ("+a.getEncargado()+")");        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listToSpinner);
        spinner.setAdapter(adapter);

        //Para SpinnerClasif
        listaClasificaciones = BDhelper.listaClasificacion();
        listToSpinnerClasif = new ArrayList<>();
        listToSpinnerClasif.add("SELECCIONE");
        for (Clasificacion c : listaClasificaciones) {
            listToSpinnerClasif.add(c.getIdClasificacion() + "-" + c.getCatalogo());
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listToSpinnerClasif);
        spinnerC.setAdapter(adapter2);


    }




    public void insertar(View v) {
        String marca=editMarca.getText().toString();
        String modelo=editModelo.getText().toString();
        String cantidad =editCantidad.getText().toString();
        String fecha=editFecha.getText().toString();

        String regInsertados;
        if(!marca.isEmpty()&&!modelo.isEmpty()&&!cantidad.isEmpty()&&!fecha.isEmpty()&& idUnidadAdmin!=0&& idClasificacion!=0) {
            Equipo equipo = new Equipo();
            equipo.setIdUnidadAdmin(idUnidadAdmin);
            equipo.setIdClasificacion(idClasificacion);
            equipo.setMarca(marca);
            equipo.setModelo(modelo);
            equipo.setCantidad(Integer.parseInt(cantidad));
            equipo.setFechaCompra(fecha);


            BDhelper.abrir();
            regInsertados = BDhelper.insertarEquipo(equipo);
            BDhelper.cerrar();
            Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Debe Conpletar todos los campos", Toast.LENGTH_SHORT).show();
        }

       /* Intent intent = new Intent(this,EquipoConsulActualiElim.class);
        intent.putExtra("indice",index);
        startActivity(intent);*/

    }

    public void Cancelar (View v){
        Intent intent = new Intent(this,EquipoMenuActivity.class );
        startActivity(intent);
    }

}
