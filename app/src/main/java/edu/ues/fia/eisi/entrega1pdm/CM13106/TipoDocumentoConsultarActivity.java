package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class TipoDocumentoConsultarActivity extends Activity {
    ControlBD helper;
    EditText editIdTipoDoc;
    EditText editCatalogo;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_tipo_documento_consultar);
        helper = new ControlBD(this);
        editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        editCatalogo = (EditText) findViewById(R.id.editCatalogo);

    }
    public void consultarTipoDocumento(View v) {
        helper.abrir();
        TipoDocumento tipoDocumento = helper.consultarTipoDocumento(Integer.parseInt(editIdTipoDoc.getText().toString()));
        helper.cerrar();
        if(tipoDocumento == null)
            Toast.makeText(this, "TipoDocumento con idTipoDoc " + editIdTipoDoc.getText().toString() +
                    " no encontrado", Toast.LENGTH_LONG).show();
        else{
            editCatalogo.setText(tipoDocumento.getCatalogo());
        }
    }
    public void limpiarTexto(View v){
        editIdTipoDoc.setText("");
        editCatalogo.setText("");
    }
}
