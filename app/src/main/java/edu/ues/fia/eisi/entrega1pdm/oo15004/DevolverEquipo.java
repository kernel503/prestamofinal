package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DevolverEquipo extends Fragment implements View.OnClickListener {
    Docente d = new Docente();
    int idDocente = d.getIdDocente();
    View v;
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    FloatingActionButton btnDel;
    ArrayList<Equipo> lista = new ArrayList<>();
    int idPrestamoEquipo = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_devolver_equipo, container, false);
        BDhelper=new ControladorBD(getActivity());

        listView = v.findViewById(R.id.listaDevolverEquipo);
        btnDel = v.findViewById(R.id.devolverEquipo);
        btnDel.setOnClickListener(this);
        btnDel.setEnabled(false);
        llenarTabla();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lista.isEmpty()){
                    idPrestamoEquipo = lista.get(index).getIdEquipo();
                    btnDel.setEnabled(true);
                }
            }
        });

        return v;
    }

    private void llenarTabla(){
        lista = BDhelper.listaPrestamoEquipo(idDocente);
        stringArrayList= new ArrayList<>();

        if (!lista.isEmpty()){
            for (Equipo a : lista){
                stringArrayList.add(a.getMarca()+"\n"+a.getModelo());
            }
        }else {
            stringArrayList.add(getString(R.string.listEmpty));
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.devolverEquipo:
                Snackbar.make(getView(), BDhelper.devolverEquipo(idPrestamoEquipo), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                btnDel.setEnabled(false);
                break;
        }
    }
}
