package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.R;

public class ControladorBD {

    private final Context context;
    private ControladorBD.DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControladorBD(Context ctx) {
        this.context = ctx;
        DBHelper = new ControladorBD.DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "prestamoFinal.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }

    }

    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar(){
        DBHelper.close();
    }

    // UTILIZADA EN SOLICITARARTICULO
    public ArrayList listaArticulo(){
        ArrayList<ArticuloOficina> lista = new ArrayList<>();
        abrir();
        try{
            Cursor c= db.rawQuery("SELECT idArticulo, nombre, cantidad FROM ArticuloOficina WHERE cantidad > 0",null);
            if(c.moveToFirst()){
                do{
                    ArticuloOficina u = new ArticuloOficina();
                    u.setIdArticulo(c.getInt(0));
                    u.setNombre(c.getString(1));
                    u.setCantidad(c.getInt(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){
        }
        cerrar();
        return  lista;
    }

    // UTILIZADA EN SOLICITARARTICULO
    public String solicitarArticulo(ArticuloOficina a){
        abrir();
        String s="";
        String[] args = new String[3];
        args[0]=""+a.getIdArticulo();
        args[1]=""+a.getIdDocente();
        args[2]="1";
        try{
            db.execSQL("INSERT INTO PrestamoArticulo('idArticulo','idDocente','activo') VALUES (?,?,?)",args);
            s=context.getString(R.string.addSQL)+" "+a.getNombre();
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    // UTILIZADA EN DEVOLVERARTICULO
    public String devolverArticulo(ArticuloOficina a){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+a.getIdPrestamoArticulo();
        try{
            db.execSQL("UPDATE PrestamoArticulo SET activo = 0 WHERE idPrestamoArticulo = ?",args);
            s=context.getString(R.string.updateSQL)+" "+a.getNombre();
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    // UTILIZADA EN DEVOLVERARTICULO
    public ArrayList listaPrestamoArticulo(int idDocente){
        ArrayList<ArticuloOficina> lista = new ArrayList<>();
        abrir();
        String[] args = new String[1];
        args[0]=""+idDocente;
        try{
            Cursor c= db.rawQuery("SELECT p.idPrestamoArticulo, " +
                    "a.nombre FROM PrestamoArticulo as p INNER JOIN ArticuloOficina " +
                    "AS a USING (idArticulo) WHERE activo = 1 and idDocente = ?",args);
            if(c.moveToFirst()){
                do{
                    ArticuloOficina u = new ArticuloOficina();
                    u.setIdPrestamoArticulo(c.getInt(0));
                    u.setNombre(c.getString(1));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }


    //LISTAR TIPODOCUMENTO
    public ArrayList spinnerTipoDoc(){
        abrir();
        ArrayList<TipoDocumento> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT idTipoDocumento, catalogo FROM TipoDocumento",null);
            if(c.moveToFirst()){
                do{
                    TipoDocumento u = new TipoDocumento();
                    u.setIdTipoDocumento(c.getInt(0));
                    u.setCatalogo(c.getString(1));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){
            Log.i(Tag, "ERROR");
        }
        cerrar();
        return lista;
    }

    //Métodos Utilizados en SolicitarDocumento
    public ArrayList listaDocumento(String idTipo){
        abrir();
        ArrayList<Documento> lista = new ArrayList<>();
        String[] args = new String[1];
        args[0] = ""+idTipo;
        try{
            Cursor c= db.rawQuery("SELECT d.idDocumento, d.titulo, a.nombre AS autor, " +
                    "e.nombre AS editorial, d.isbm, d.cantidad FROM Documento AS d " +
                    "INNER JOIN TipoDocumento AS p USING (idTipoDocumento) " +
                    "INNER JOIN Autor AS a USING (idAutor) INNER JOIN Editorial " +
                    "AS e USING (idEditorial) WHERE cantidad > 0 AND idTipoDocumento = ?",args);
            if(c.moveToFirst()){
                do{
                    Documento u = new Documento();
                    u.setIdDocumento(c.getInt(0));
                    u.setNombreDocumento(c.getString(1));
                    u.setNombreAutor(c.getString(2));
                    u.setNombreEditorial(c.getString(3));
                    u.setIsbm(c.getString(4));
                    u.setCantidad(c.getInt(5));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){
            Log.i(Tag, "ERROR");
        }
        cerrar();
        return lista;
    }

    public String solicitarDocumento(Documento a){
        abrir();
        String s="";
        String[] args = new String[3];
        args[0]=""+a.getIdDocumento();
        args[1]=""+a.getIdDocente();
        args[2]="1";
        try{
            db.execSQL("INSERT INTO PrestamoDocumento('idDocumento','idDocente','activo') VALUES (?,?,?)",args);
            s=context.getString(R.string.addSQL)+" "+a.getNombreDocumento();
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    //Métodos Utilizados en DevolverDocumento
    public ArrayList listaPrestamoDocumento(int idDocente, String idTipoDoc){
        ArrayList<Documento> lista = new ArrayList<>();
        abrir();
        String[] args = new String[2];
        args[0]=""+idDocente;
        args[1]=""+idTipoDoc;
        try{
            Cursor c= db.rawQuery("SELECT i.idPrestamoDocumento, d.titulo, a.nombre, " +
                    "e.nombre,d.isbm  FROM PrestamoDocumento AS i INNER JOIN Documento AS " +
                    "d USING (idDocumento) INNER JOIN Autor as a USING (idAutor) " +
                    "INNER JOIN Editorial as e USING (idEditorial) INNER JOIN TipoDocumento " +
                    "AS td USING (idTipoDocumento) WHERE i.activo = 1 AND idDocente = ? " +
                    "AND td.idTipoDocumento = ? ",args);
            if(c.moveToFirst()){
                do{
                    Documento u = new Documento();
                    u.setIdPrestamoDocumento(c.getInt(0));
                    u.setNombreDocumento(c.getString(1));
                    u.setNombreAutor(c.getString(2));
                    u.setNombreEditorial(c.getString(3));
                    u.setIsbm(c.getString(4));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String devolverDocumento(Documento d){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+d.getIdPrestamoDocumento();
        try{
            db.execSQL("UPDATE PrestamoDocumento SET activo = 0 WHERE idPrestamoDocumento = ?",args);
            s=context.getString(R.string.updateSQL)+" "+d.getNombreDocumento();
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    //SOLOCITAR EQUIPO
    public ArrayList listaUnidadAdmin(){
        abrir();
        ArrayList<UnidadAdmin> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT idUnidadAdmin, escuela, unidad FROM UnidadAdmin", null);
            if(c.moveToFirst()){
                do{
                    UnidadAdmin u = new UnidadAdmin();
                    u.setIdUnidadAdmin(c.getInt(0));
                    u.setEscuela(c.getString(1));
                    u.setUnidad(c.getString(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public ArrayList listaGrupo(){
        abrir();
        ArrayList<Grupo> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT idGrupo, lugar, capacidad FROM GrupoAsignado", null);
            if(c.moveToFirst()){
                do{
                    Grupo u = new Grupo();
                    u.setIdGrupo(c.getInt(0));
                    u.setLugar(c.getString(1));
                    u.setCapacidad(c.getString(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public ArrayList listaMateria(){
        abrir();
        ArrayList<Materia> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT idMateria, codMateria, nombre FROM Materia", null);
            if(c.moveToFirst()){
                do{
                    Materia u = new Materia();
                    u.setIdMateria(c.getInt(0));
                    u.setCodMateria(c.getString(1));
                    u.setNombre(c.getString(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public ArrayList listaClasificacion(){
        abrir();
        ArrayList<Clasificacion> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT idClasificacion, catalogo FROM Clasificacion", null);
            if(c.moveToFirst()){
                do{
                    Clasificacion u = new Clasificacion();
                    u.setIdClasificacion(c.getInt(0));
                    u.setCatalogo(c.getString(1));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public ArrayList equipoFiltrado(int idUnidad, int idClasificacion){
        abrir();
        ArrayList<Equipo> lista = new ArrayList<>();
        String[] args = new String[2];
        args[0]=""+idUnidad;
        args[1]=""+idClasificacion;
        try{
            Cursor c= db.rawQuery("SELECT idEquipo, marca, modelo FROM " +
                    "Equipo WHERE idUnidadAdmin = ? AND idClasificacion = ? ", args);
            if(c.moveToFirst()){
                do{
                    Equipo u = new Equipo();
                    u.setIdEquipo(c.getInt(0));
                    u.setMarca(c.getString(1));
                    u.setModelo(c.getString(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }

        cerrar();
        return lista;
    }

    public ArrayList horarioDisponible(int idEquipo){
        abrir();
        ArrayList<Horario> lista = new ArrayList<>();
        String[] args = new String[1];
        args[0]=""+idEquipo;
        try{
            Cursor c= db.rawQuery("SELECT idHorario, dia, inicio, fin FROM Horario WHERE " +
                    "idHorario NOT IN (SELECT idHorario FROM (SELECT * FROM PrestamoEquipo AS p " +
                    "INNER JOIN Horario AS h WHERE p.espacio1=h.idHorario OR " +
                    "p.espacio2=h.idHorario)WHERE activo = 1 AND idEquipo = ? ORDER BY idHorario ASC)", args);
            if(c.moveToFirst()){
                do{
                    Horario u = new Horario();
                    u.setIdHorario(c.getInt(0));
                    u.setDia(c.getString(1)+" "+c.getString(2)+" - "+c.getString(3));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public String solicitarEquipo(int idEquipo, int idGrupo, int idDocente, int idMateria, int sp1, int sp2){
        abrir();
        String valor = "";
        String[] args = new String[7];
        args[0]=""+idEquipo;
        args[1]=""+idGrupo;
        args[2]=""+idDocente;
        args[3]=""+idMateria;
        args[4]=""+sp1;
        args[5]=""+sp2;
        args[6]=""+1;
        try{
            db.execSQL("INSERT INTO PrestamoEquipo ('idEquipo', 'idGrupo', 'idDocente', " +
                    "'idMateria','espacio1','espacio2','activo') VALUES (?,?,?,?,?,?,?)",args);
            valor=context.getString(R.string.addSQL);
        }catch (SQLException ex){
            valor=context.getString(R.string.failSQL);
        }
        cerrar();
        return valor;
    }
    public ArrayList listaPrestamoEquipo(int idDocente){
        ArrayList<Equipo> lista = new ArrayList<>();
        abrir();
        String[] args = new String[1];
        args[0]=""+idDocente;
        try{
            Cursor c= db.rawQuery("SELECT p.idPrestamoEquipo, c.catalogo, e.marca, e.modelo, " +
                    "m.nombre, m.codMateria, g.lugar, h1.dia, h1.inicio, h1.fin, h2.dia, " +
                    "h2.inicio, h2.fin  FROM PrestamoEquipo as p " +
                    "INNER JOIN Equipo AS e USING (idEquipo) " +
                    "INNER JOIN GrupoAsignado AS g USING (idGrupo) " +
                    "INNER JOIN Materia AS m USING (idMateria) " +
                    "INNER JOIN Docente AS d USING (idDocente) " +
                    "INNER JOIN Horario as h1 ON p.espacio1 = h1.idHorario " +
                    "INNER JOIN Horario as h2 ON p.espacio2 = h2.idHorario " +
                    "INNER JOIN Clasificacion AS c USING (idClasificacion) " +
                    "WHERE idDocente = ? AND p.activo = 1",args);
            if(c.moveToFirst()){
                do{
                    Equipo u = new Equipo();
                    u.setIdEquipo(c.getInt(0));

                    u.setMarca(c.getString(1)+"\n"+c.getString(2)+" "+
                            c.getString(3)+"\n"+c.getString(4)
                    +" ("+c.getString(5)+")"+"\n"+c.getString(6));

                    u.setModelo(c.getString(7)+": "+c.getString(8)
                    +" - "+c.getString(9)+"\n"+c.getString(10)
                            +": "+c.getString(11)+" - "+c.getString(12));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String devolverEquipo(int idPrestamoEquipo){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+idPrestamoEquipo;
        try{
            db.execSQL("UPDATE PrestamoEquipo SET activo = 0 WHERE idPrestamoEquipo = ?",args);
            s=context.getString(R.string.updateSQL);
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    //LISTA ACTIVO DE ARTICULO
    public ArrayList listaActivoArticulo(){
        ArrayList<ArticuloOficina> lista = new ArrayList<>();
        abrir();
        try{
            Cursor c= db.rawQuery("SELECT p.idPrestamoArticulo, d.nombre, a.nombre " +
                    "FROM PrestamoArticulo AS p " +
                    "INNER JOIN Docente AS d USING (idDocente) " +
                    "INNER JOIN ArticuloOficina AS a USING (idArticulo) " +
                    "WHERE p.activo = 1 " +
                    "ORDER BY p.idDocente ASC, a.idArticulo ASC ",null);
            if(c.moveToFirst()){
                do{
                    ArticuloOficina u = new ArticuloOficina();
                    u.setIdPrestamoArticulo(c.getInt(0));
                    u.setNombre(c.getString(1)+"\n"+c.getString(2));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String devolverActivoArticulo(int idPrestamoArticulo){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+idPrestamoArticulo;
        try{
            db.execSQL("UPDATE PrestamoArticulo SET activo = 0 WHERE idPrestamoArticulo = ?",args);
            s=context.getString(R.string.updateSQL);
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    //LISTA ACTIVO DE DOCUMENTOS
    public ArrayList listaActivoDocumento(){
        ArrayList<Documento> lista = new ArrayList<>();
        abrir();
        try{
            Cursor c= db.rawQuery("SELECT p.idPrestamoDocumento, c.nombre, t.catalogo, " +
                    "d.titulo, d.isbm FROM PrestamoDocumento AS p " +
                    "INNER JOIN Documento AS d USING (idDocumento) " +
                    "INNER JOIN Docente AS c USING (idDocente) " +
                    "INNER JOIN TipoDocumento AS t USING (idTipoDocumento) " +
                    "WHERE p.activo = 1  " +
                    "ORDER BY idDocente ASC, idTipoDocumento ASC",null);
            if(c.moveToFirst()){
                do{
                    Documento u = new Documento();
                    u.setIdPrestamoDocumento(c.getInt(0));
                    u.setNombreDocumento(c.getString(1)+"\n"+c.getString(2));
                    u.setNombreAutor(c.getString(3)+"\n"+c.getString(4));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String devolverActivoDocumento(int idPrestamoDocumento){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+idPrestamoDocumento;
        try{
            db.execSQL("UPDATE PrestamoDocumento SET activo = 0 WHERE idPrestamoDocumento = ?",args);
            s=context.getString(R.string.updateSQL);
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    //LISTA ACTIVO DE DOCUMENTOS
    public ArrayList listaActivoEquipo(){
        ArrayList<Equipo> lista = new ArrayList<>();
        abrir();
        try{
            Cursor c= db.rawQuery("SELECT p.idPrestamoEquipo, d.nombre, cl.catalogo, e.marca, " +
                    "m.nombre, m.codMateria, h1.dia, h1.inicio, h1.fin, h2.dia, h2.inicio, " +
                    "h2.fin FROM PrestamoEquipo AS p " +
                    "INNER JOIN Equipo as e USING (idEquipo) " +
                    "INNER JOIN Docente as d USING (idDocente) " +
                    "INNER JOIN GrupoAsignado as g USING (idGrupo) " +
                    "INNER JOIN Horario AS h1 ON p.espacio1 = h1.idHorario " +
                    "INNER JOIN Horario AS h2 ON p.espacio2 = h2.idHorario " +
                    "INNER JOIN Materia AS m USING (idMateria) " +
                    "INNER JOIN Clasificacion AS cl USING (idClasificacion) " +
                    "WHERE p.activo = 1 " +
                    "ORDER BY p.idDocente ASC, cl.idClasificacion ASC",null);
            if(c.moveToFirst()){
                do{
                    Equipo u = new Equipo();
                    u.setIdEquipo(c.getInt(0));
                    u.setMarca(c.getString(1)+"\n"+c.getString(2)+" ("+c.getString(3)+")\n"
                    +c.getString(4)+" - " + c.getString(5));
                    u.setModelo(c.getString(6)+": "+c.getString(7)+" - "+c.getString(8)
                    +"\n"+c.getString(9)+": "+c.getString(10)+" - "+c.getString(11));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String devolverActivoEquipo(int idPrestamoEquipo){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+idPrestamoEquipo;
        try{
            db.execSQL("UPDATE PrestamoEquipo SET activo = 0 WHERE idPrestamoEquipo = ?",args);
            s=context.getString(R.string.updateSQL);
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    public ArrayList listaHorario(){
        ArrayList<Horario> lista = new ArrayList<>();
        abrir();
        try{
            Cursor c= db.rawQuery("SELECT * FROM Horario",null);
            if(c.moveToFirst()){
                do{
                    Horario u = new Horario();
                    u.setIdHorario(c.getInt(0));
                    u.setDia(c.getString(1));
                    u.setInicio(c.getString(2));
                    u.setFin(c.getString(3));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  lista;
    }

    public String insertarHorario(String dia, String inicio, String fin){
        abrir();
        String s="";
        String[] args = new String[3];
        args[0]=""+dia;
        args[1]=""+inicio;
        args[2]=""+fin;
        try{
            db.execSQL("INSERT INTO Horario(dia, inicio, fin) VALUES (?,?,?)",args);
            s=context.getString(R.string.addSQL);
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    public String actualizarHorario(String dia, String inicio, String fin, int idHorario){
        abrir();
        String s="";
        String[] args = new String[4];
        args[0]=""+dia;
        args[1]=""+inicio;
        args[2]=""+fin;
        args[3]=""+idHorario;
        try{
            db.execSQL("UPDATE Horario SET dia = ?, inicio = ?, fin = ? WHERE idHorario = ?",args);
            s="Registro Actualizado "+idHorario;
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

    public String eliminarHorario(int idHorario){
        abrir();
        String s="";
        String[] args = new String[1];
        args[0]=""+idHorario;
        try{
            db.execSQL("DELETE FROM Horario WHERE idHorario = ?",args);
            s="Registro Eliminado "+idHorario;
        }catch (SQLException ex){
            s=context.getString(R.string.failSQL);
        }
        cerrar();
        return s;
    }

}

