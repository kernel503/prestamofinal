package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class AutorConsultarActivity extends Activity {

    ControlBD helper;
    EditText editIdAutor;
    EditText editNombre;
    EditText editNacionalidad;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_autor_consultar);
        helper = new ControlBD(this);
        editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        editNombre = (EditText) findViewById(R.id.editNombre);
        editNacionalidad = (EditText) findViewById(R.id.editNacionalidad);
    }
    public void consultarAutor(View v) {
        helper.abrir();
        Autor autor = helper.consultarAutor(Integer.parseInt(editIdAutor.getText().toString()));
        helper.cerrar();
        if(autor == null)
            Toast.makeText(this, "Autor con idAutor " + editIdAutor.getText().toString() +
                    " no encontrado", Toast.LENGTH_LONG).show();
        else{
            editNombre.setText(autor.getNombre());
            editNacionalidad.setText(autor.getNacionalidad());
        }
    }
    public void limpiarTexto(View v){
        editIdAutor.setText("");
        editNombre.setText("");
        editNacionalidad.setText("");
    }
}
