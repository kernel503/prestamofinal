package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;

public class SolicitarEquipo extends Fragment implements View.OnClickListener {

    Docente d = new Docente();
    int idDocente = d.getIdDocente();

    View v;
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;

    ArrayList<UnidadAdmin> listaUnidad = new ArrayList<>();
    ArrayList<Grupo> listaGrupo = new ArrayList<>();
    ArrayList<Materia> listaMateria = new ArrayList<>();
    ArrayList<Clasificacion> listaClasificacion = new ArrayList<>();

    ArrayList<Equipo> lstEquipo = new ArrayList<>();
    ArrayList<Horario> lstDisponible = new ArrayList<>();

    Spinner spUnidadAdmin, spGrupo, spMateria, spClasificacion;
    FloatingActionButton btnAdd;
    private ListView listView;

    int idClasificacionPrestamo =0, idUnidadAdminPrestamo=0;
    int idGrupoPrestamo, idMateriaPrestamo, idEquipoPrestamo;
    int sche1=0, sche2=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_solicitar_equipo, container, false);
        BDhelper=new ControladorBD(getActivity());
        spUnidadAdmin = (Spinner) v.findViewById(R.id.spUnidadAdmin);
        spGrupo = (Spinner) v.findViewById(R.id.spGrupo);
        spMateria = (Spinner) v.findViewById(R.id.spMateria);
        spClasificacion = (Spinner) v.findViewById(R.id.spClasificacion);
        btnAdd = v.findViewById(R.id.agregarEquipo);
        btnAdd.setOnClickListener(this);
        listView = v.findViewById(R.id.lstEquipo);

        spinnerUnidad();
        spinnerGrupo();
        spinnerMateria();
        spinnerClasificacion();

        spUnidadAdmin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        btnAdd.setEnabled(false);
                        idUnidadAdminPrestamo = listaUnidad.get(pos).getIdUnidadAdmin();
                        llenarTabla(idUnidadAdminPrestamo, idClasificacionPrestamo);
                        if (lstEquipo.isEmpty()){
                            btnAdd.setEnabled(false);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        spClasificacion.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        btnAdd.setEnabled(false);
                        idClasificacionPrestamo = listaClasificacion.get(pos).getIdClasificacion();
                        llenarTabla(idUnidadAdminPrestamo, idClasificacionPrestamo);
                        if (lstEquipo.isEmpty()){
                            btnAdd.setEnabled(false);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        spGrupo.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        idGrupoPrestamo = listaGrupo.get(pos).getIdGrupo();
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        spMateria.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        idMateriaPrestamo = listaMateria.get(pos).getIdMateria();
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lstEquipo.isEmpty()){
                    idEquipoPrestamo = lstEquipo.get(index).getIdEquipo();
                    btnAdd.setEnabled(true);
                }

            }
        });

        return v;
    }

    public void spinnerUnidad(){
        listaUnidad = BDhelper.listaUnidadAdmin();
        stringArrayList= new ArrayList<>();
        for (UnidadAdmin a : listaUnidad){
            stringArrayList.add(a.getEscuela()+" ("+a.getUnidad()+")");
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUnidadAdmin.setAdapter(spinnerArrayAdapter);
    }

    public void spinnerGrupo(){
        listaGrupo = BDhelper.listaGrupo();
        stringArrayList= new ArrayList<>();
        for (Grupo a : listaGrupo){
            stringArrayList.add(a.getLugar()+" "+getString(R.string.capacidad)+" "+a.getCapacidad());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGrupo.setAdapter(spinnerArrayAdapter);
    }

    public void spinnerMateria(){
        listaMateria = BDhelper.listaMateria();
        stringArrayList= new ArrayList<>();
        for (Materia a : listaMateria){
            stringArrayList.add(a.getCodMateria()+" -  "+a.getNombre());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMateria.setAdapter(spinnerArrayAdapter);
    }

    public void spinnerClasificacion(){
        listaClasificacion = BDhelper.listaClasificacion();
        stringArrayList= new ArrayList<>();
        for (Clasificacion a : listaClasificacion){
            stringArrayList.add(a.getCatalogo());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spClasificacion.setAdapter(spinnerArrayAdapter);
    }

    private void llenarTabla(int idUni, int idCla){
        lstEquipo= BDhelper.equipoFiltrado(idUni,idCla);
        stringArrayList= new ArrayList<>();
        if(!lstEquipo.isEmpty()){
            for (Equipo a : lstEquipo){
                stringArrayList.add(a.getMarca() +" "+ a.getModelo());
            }
        }else{
            stringArrayList.add(getString(R.string.listEmpty));
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.agregarEquipo:
                showAlertDialog();
                //Snackbar.make(getView(), BDhelper.solicitarDocumento(d), Snackbar.LENGTH_LONG)
                 //       .setAction("Action", null).show();
                break;
        }
    }

    public void showAlertDialog() {
        lstDisponible=BDhelper.horarioDisponible(idEquipoPrestamo);

        stringArrayList= new ArrayList<>();
        if(!lstDisponible.isEmpty()){
            for (Horario a : lstDisponible){
                stringArrayList.add(a.getDia());
            }
        }else{
            stringArrayList.add(getString(R.string.listEmpty));
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,stringArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        Context context = getContext();
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView h = new TextView(context);
        h.setText("Seleccionar Horario");
        h.setPadding(5,5,0,5);
        layout.addView(h);

        final TextView h1 = new TextView(context);
        h1.setPadding(5,5,0,5);
        h1.setText("Horario 1");
        layout.addView(h1);

        final Spinner spHorario1 = new Spinner(context);
        spHorario1.setPadding(5,5,5,5);
        spHorario1.setAdapter(spinnerArrayAdapter);
        layout.addView(spHorario1);

        final TextView h2 = new TextView(context);
        h2.setPadding(5,5,0,5);
        h2.setText("Horario 2");
        layout.addView(h2);

        final Spinner spHorario2 = new Spinner(context);
        spHorario2.setPadding(5,5,5,5);
        spHorario2.setAdapter(spinnerArrayAdapter);
        layout.addView(spHorario2);

        builder.setView(layout);

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sche1 = lstDisponible.get(spHorario1.getSelectedItemPosition()).getIdHorario();
                sche2 = lstDisponible.get(spHorario2.getSelectedItemPosition()).getIdHorario();
                if (sche1 == sche2){
                    Toast.makeText(getActivity(),getString(R.string.sameSchedule),Toast.LENGTH_SHORT).show();
                    showAlertDialog();
                }else{
                    Toast.makeText(getActivity(),BDhelper.solicitarEquipo(idEquipoPrestamo,idGrupoPrestamo,idDocente,idMateriaPrestamo,sche1,sche2),Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }


}

