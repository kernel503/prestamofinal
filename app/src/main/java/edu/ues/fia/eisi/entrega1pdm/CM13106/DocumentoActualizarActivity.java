package edu.ues.fia.eisi.entrega1pdm.CM13106;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DocumentoActualizarActivity extends Activity {
    private static final String Tag = "Mensajes";

    ControlBD helper;
    EditText editIdDocumento;
    EditText editIdAutor;
    EditText editIdTipoDoc;
    EditText editTitulo;
    EditText editISBM;
    EditText editIdioma;
    EditText editDetalle;
    EditText editCantidad;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento_actualizar);
        helper = new ControlBD(this);
        editIdDocumento = (EditText) findViewById(R.id.editIdDocumento);
        editTitulo = (EditText) findViewById(R.id.editTitulo);
        editISBM = (EditText) findViewById(R.id.editISBM);
        editIdioma = (EditText) findViewById(R.id.editIdioma);
        editCantidad = (EditText) findViewById(R.id.editCantidad);

        //editIdAutor = (EditText) findViewById(R.id.editIdAutor);
        //editIdTipoDoc = (EditText) findViewById(R.id.editIdTipoDoc);
        //editDetalle = (EditText) findViewById(R.id.editDetalle);

    }
    public void actualizarDocumento(View v) {

        Log.i(Tag, "CLICK ACTUALIZAR");
        Documento documento = new Documento();

        //documento.setIdAutor(Integer.parseInt(editIdAutor.getText().toString()));
        //documento.setIdTipoDoc(Integer.parseInt(editIdTipoDoc.getText().toString()));
        //documento.setDetalle(editDetalle.getText().toString());

        if (editIdDocumento.getText().toString().isEmpty() && editCantidad.getText().toString().isEmpty() ){
            Toast.makeText(this, "Debe llenar los campos", Toast.LENGTH_SHORT).show();
        }else{
            documento.setIdDocumento(Integer.parseInt(editIdDocumento.getText().toString()));
            documento.setTitulo(editTitulo.getText().toString());
            documento.setISBM(editISBM.getText().toString());
            documento.setIdioma(editIdioma.getText().toString());
            documento.setCantidad(Integer.parseInt(editCantidad.getText().toString()));

            helper.abrir();
            String estado = helper.actualizar(documento);
            helper.cerrar();

            Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
        }
    }
    public void limpiarTexto(View v) {
        editIdDocumento.setText("");
        editTitulo.setText("");
        editISBM.setText("");
        editIdioma.setText("");
        editCantidad.setText("");


        //editIdAutor.setText("");
        //editIdTipoDoc.setText("");
        //editDetalle.setText("");
    }
}
