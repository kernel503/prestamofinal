package edu.ues.fia.eisi.entrega1pdm.oo15004;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;

public class DevolverArticulo extends Fragment implements View.OnClickListener {
    Docente d = new Docente();
    int idDocente = d.getIdDocente();

    int idPrestamoArticulo = 0;
    String nombre="";
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    FloatingActionButton btnDel;
    ArrayList<ArticuloOficina> lista = new ArrayList<>();

    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_devolver_articulo, container, false);
        BDhelper=new ControladorBD(getActivity());
        listView = v.findViewById(R.id.listaDevolucionArticulo);
        btnDel = v.findViewById(R.id.devolverArticulo);
        btnDel.setOnClickListener(this);
        btnDel.setEnabled(false);

        llenarTabla();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                if(!lista.isEmpty()){
                    idPrestamoArticulo = lista.get(index).getIdPrestamoArticulo();
                    nombre = lista.get(index).getNombre();
                    btnDel.setEnabled(true);
                }
            }
        });
        return v;
    }

    private void llenarTabla(){
        lista = BDhelper.listaPrestamoArticulo(idDocente);
        stringArrayList= new ArrayList<>();

        if (!lista.isEmpty()){
            for (ArticuloOficina a : lista){
                stringArrayList.add(getString(R.string.listaArticulo)+" "+a.getNombre());
            }
        }else {
                stringArrayList.add(getString(R.string.listEmpty));
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.devolverArticulo:
                ArticuloOficina a = new ArticuloOficina();
                a.setIdPrestamoArticulo(idPrestamoArticulo);
                a.setNombre(nombre);
                Snackbar.make(getView(), BDhelper.devolverArticulo(a), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                btnDel.setEnabled(false);
                break;
        }
    }
}
