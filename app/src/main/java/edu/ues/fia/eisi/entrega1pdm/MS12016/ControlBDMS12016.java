package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.MS12016.ArticuloOficina;
import edu.ues.fia.eisi.entrega1pdm.MS12016.DetalleSustitucion;
import edu.ues.fia.eisi.entrega1pdm.MS12016.Editorial;


public class ControlBDMS12016 {
    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControlBDMS12016(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "prestamoFinal.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                /*
                db.execSQL("CREATE TABLE ArticuloOficina (idArticulo INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, distribuidor TEXT NOT NULL, cantidad INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE Autor (idAutor INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, nacionalidad TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Clasificacion (idClasificacion INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL, activo INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE DetalleSustitucion (idSustitucion INTEGER PRIMARY KEY AUTOINCREMENT, motivo TEXT NOT NULL, descripcion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Docente (idDocente INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL, cargo TEXT NOT NULL, fechaContratacion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Documento (idDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idTipoDocumento INTEGER NOT NULL, idAutor INTEGER NOT NULL, idEditorial INTEGER NOT NULL, titulo TEXT NOT NULL, isbm TEXT NOT NULL, idioma TEXT NOT NULL, cantidad INTEGER NOT NULL, FOREIGN KEY(idEditorial) REFERENCES Editorial, FOREIGN KEY(idTipoDocumento) REFERENCES TipoDocumento, FOREIGN KEY(idAutor) REFERENCES Autor);");
                db.execSQL("CREATE TABLE Editorial (idEditorial INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT NOT NULL,direccion TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Equipo (idEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idClasificacion INTEGER NOT NULL, idUnidadAdmin	INTEGER NOT NULL, marca TEXT NOT NULL, modelo TEXT NOT NULL, cantidad INTEGER NOT NULL, fechaCompra TEXT NOT NULL, activo INTEGER NOT NULL, FOREIGN KEY(idClasificacion) REFERENCES Clasificacion, FOREIGN KEY(idUnidadAdmin) REFERENCES UnidadAdmin);");
                db.execSQL("CREATE TABLE GrupoAsignado (idGrupo INTEGER PRIMARY KEY AUTOINCREMENT, lugar TEXT NOT NULL, capacidad INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE Horario (idHorario INTEGER PRIMARY KEY AUTOINCREMENT, dia TEXT NOT NULL, inicio TEXT NOT NULL, fin TEXT NOT NULL);");
                db.execSQL("CREATE TABLE Inventario (idInventario INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER, idDocumento INTEGER,idArticulo INTEGER,cantidadInicial INTEGER NOT NULL,cantidadFinal INTEGER NOT NULL, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina);");
                db.execSQL("CREATE TABLE Materia (idMateria INTEGER PRIMARY KEY AUTOINCREMENT, codMateria TEXT NOT NULL, nombre TEXT NOT NULL);");
                db.execSQL("CREATE TABLE TipoDocumento (idTipoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, catalogo TEXT NOT NULL);");
                db.execSQL("CREATE TABLE UnidadAdmin (idUnidadAdmin INTEGER PRIMARY KEY AUTOINCREMENT, escuela TEXT NOT NULL, unidad TEXT NOT NULL, encargado TEXT NOT NULL, activo INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE PrestamoArticulo (idPrestamoArticulo INTEGER PRIMARY KEY AUTOINCREMENT, idArticulo INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idArticulo) REFERENCES ArticuloOficina, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                db.execSQL("CREATE TABLE PrestamoDocumento (idPrestamoDocumento INTEGER PRIMARY KEY AUTOINCREMENT, idDocumento INTEGER NOT NULL, idDocente INTEGER NOT NULL, activo INTEGER NOT NULL, idSustitucion INTEGER, FOREIGN KEY(idDocumento) REFERENCES Documento, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                db.execSQL("CREATE TABLE PrestamoEquipo (idPrestamoEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idEquipo INTEGER NOT NULL, idGrupo INTEGER NOT NULL, idDocente INTEGER NOT NULL, idMateria INTEGER NOT NULL, activo INTEGER NOT NULL, espacio1 INTEGER, espacio2 INTEGER, espacio3 INTEGER, idSustitucion INTEGER, FOREIGN KEY(idMateria) REFERENCES Materia, FOREIGN KEY(idGrupo) REFERENCES GrupoAsignado, FOREIGN KEY(idDocente) REFERENCES Docente, FOREIGN KEY(idEquipo) REFERENCES Equipo, FOREIGN KEY(idSustitucion) REFERENCES DetalleSustitucion);");
                */

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

    }

    public void abrir() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar() {
        DBHelper.close();
    }

    public String insertarArticuloOficina(ArticuloOficina a) {
        String retorno;
        try {
            Cursor c = db.rawQuery("SELECT idArticulo FROM ArticuloOficina WHERE nombre " +
                   "like '" + a.getNombre() + "' and cantidad like '" + a.getCantidad() + "' and distribuidor like '" + a.getDistribuidor() + "'", null);
            try {
                db.execSQL("INSERT INTO ArticuloOficina('nombre','cantidad','distribuidor') VALUES( '" + a.getNombre() + "', " + String.valueOf(a.getCantidad()) + ",'" + a.getDistribuidor() + "')");

                retorno = "Se ingreso: " + a.getNombre() + " - " + a.getCantidad() + "-" + a.getDistribuidor();
            } catch (SQLException ex) {
                retorno = "Ha ocurrido un error en la conexion";
            }
        } catch (SQLException ex) {
            retorno = "ERROR SINTAXSIS";
        }
        return retorno;
    }

    public String eliminar(ArticuloOficina articulo){
        String retorno="";
        try {
            db.execSQL("DELETE FROM ArticuloOficina WHERE idArticulo = " + String.valueOf(articulo.getIdArticulo()));

            retorno="Se borro: "+articulo.getNombre();
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public String actualizar(ArticuloOficina articulo){
        String retorno="";
        try {
            db.execSQL("UPDATE ArticuloOficina SET " +
                    "nombre = '"+articulo.getNombre()+"' , " +
                    "cantidad = " + String.valueOf(articulo.getCantidad())+", "+
                    "distribuidor= '"+articulo.getDistribuidor()+"' WHERE idArticulo = " + String.valueOf(articulo.getIdArticulo()));
            retorno="Se actualizo";
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public ArrayList listaArticuloOficina(){
        ArrayList<ArticuloOficina> lista = new ArrayList<>();
        abrir();
        Cursor c= db.rawQuery("SELECT * FROM ArticuloOficina",null);


        if(c.moveToFirst()){
            do{
                ArticuloOficina a = new ArticuloOficina();
                a.setIdArticulo(Integer.parseInt(c.getString(0)));
                a.setNombre(c.getString(1));
                a.setCantidad(Integer.parseInt(c.getString(3)));
                a.setDistribuidor(c.getString(2));
                lista.add(a);
            }while (c.moveToNext());{
            }
        }
        cerrar();
        return lista;
    }

    public String insertarDetalleSustitucion(DetalleSustitucion d) {
        String retorno;
        try {
            Cursor c = db.rawQuery("SELECT idSustitucion FROM DetalleSustitucion WHERE motivo " +
                    "like '" + d.getMotivo() + "' and descripcion like '" + d.getDescripcion() + "'", null);
            try {
                db.execSQL("INSERT INTO DetalleSustitucion('motivo','descripcion') VALUES( '" + d.getMotivo() + "', '" + d.getDescripcion() + "')");

                retorno = "Se ingreso: " + d.getMotivo() + " - " + d.getDescripcion();
            } catch (SQLException ex) {
                retorno = "Ha ocurrido un error en la conexion";
            }
        } catch (SQLException ex) {
            retorno = "ERROR SINTAXSIS";
        }
        return retorno;
    }

    public String eliminar(DetalleSustitucion detalleSustitucion){
        String retorno="";
        try {
            db.execSQL("DELETE FROM DetalleSustitucion WHERE idSustitucion = "+String.valueOf(detalleSustitucion.getIdSustitucion()));

            retorno="Se borro: "+detalleSustitucion.getMotivo();
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public String actualizar(DetalleSustitucion detalleSustitucion){
        String retorno="";
        try {
            db.execSQL("UPDATE DetalleSustitucion SET " +
                    "motivo = '"+detalleSustitucion.getMotivo()+"' , " +
                    "descripcion = '"+detalleSustitucion.getDescripcion()+"' WHERE idSustitucion = " + String.valueOf(detalleSustitucion.getIdSustitucion()));
            retorno="Se actualizo";
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public ArrayList listaDetalleSustitucion(){
        ArrayList<DetalleSustitucion> lista = new ArrayList<>();
        abrir();
        Cursor c= db.rawQuery("SELECT * FROM DetalleSustitucion",null);


        if(c.moveToFirst()){
            do{
                DetalleSustitucion a = new DetalleSustitucion();
                a.setIdSustitucion(Integer.parseInt(c.getString(0)));
                a.setMotivo(c.getString(1));;
                a.setDescripcion(c.getString(2));
                lista.add(a);
            }while (c.moveToNext());{
            }
        }
        cerrar();
        return lista;
    }

    public String insertarEditorial(Editorial e) {
        String retorno;
        try {
            Cursor c = db.rawQuery("SELECT idEditorial FROM Editorial WHERE nombre " +
                    "like '" + e.getNombre() + "' and direccion like '" + e.getDireccion() +"'", null);
            try {
                db.execSQL("INSERT INTO Editorial('nombre','direccion') VALUES( '" + e.getNombre() + "', '" + e.getDireccion() + "')");

                retorno = "Se ingreso: " + e.getNombre() + " - " + e.getDireccion() ;
            } catch (SQLException ex) {
                retorno = "Ha ocurrido un error en la conexion";
            }
        } catch (SQLException ex) {
            retorno = "ERROR SINTAXSIS";
        }
        return retorno;
    }

    public String eliminar(Editorial editorial){
        String retorno="";
        try {
            db.execSQL("DELETE FROM Editorial WHERE idEditorial = " + String.valueOf(editorial.getIdEditorial()));

            retorno="Se borro: "+editorial.getNombre();
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public String actualizar(Editorial editorial){
        String retorno="";
        try {
            db.execSQL("UPDATE Editorial SET " +
                    "nombre = '"+editorial.getNombre()+"' , " +
                    "direccion = '"+editorial.getDireccion()+"' WHERE idEditorial = " + String.valueOf(editorial.getIdEditorial()));
            retorno="Se actualizo";
        }catch (SQLException ex){
            retorno = "Ha ocurrido un error en la conexion";
        }
        return  retorno;
    }

    public ArrayList listaEditorial(){
        ArrayList<Editorial> lista = new ArrayList<>();
        abrir();
        Cursor c= db.rawQuery("SELECT * FROM Editorial",null);


        if(c.moveToFirst()){
            do{
                Editorial e = new Editorial();
                e.setIdEditorial(Integer.parseInt(c.getString(0)));
                e.setNombre(c.getString(1));
                e.setDireccion(c.getString(2));
                lista.add(e);
            }while (c.moveToNext());{
            }
        }
        cerrar();
        return lista;
    }



    public String llenarBD(){
        String valor ="";
        abrir();
        try {
            db.execSQL("DROP TABLE ArticuloOficina");
            db.execSQL("CREATE TABLE ArticuloOficina (idArticulo INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, nombre VARCHAR(40)  NOT NULL UNIQUE, cantidad INTEGER   NOT NULL , distribuidor VARCHAR(40) NOT NULL UNIQUE);");
            db.execSQL("DROP TABLE DetalleSustitucion");
            db.execSQL("CREATE TABLE DetalleSustitucion (idSustitucion INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, motivo VARCHAR(40)  NOT NULL UNIQUE, descripcion  VARCHAR(40) NOT NULL UNIQUE);");
            db.execSQL("DROP TABLE Editorial");
            db.execSQL("CREATE TABLE Editorial (idEditorial INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, nombre VARCHAR(40)  NOT NULL UNIQUE,  descripcion VARCHAR(40) NOT NULL UNIQUE);");
            // db.execSQL("DROP TABLE Equipo");
           // db.execSQL("CREATE TABLE Equipo (idEquipo INTEGER PRIMARY KEY AUTOINCREMENT, idUnidadAdmin INTEGER , marca VARCHAR(40) NOT NULL, modelo VARCHAR(40) NOT NULL, cantidad INTEGER NOT NULL, fechaCompra DATE NOT NULL, activo INTEGER NOT NULL, FOREIGN KEY(idUnidadAdmin) REFERENCES UnidadAdmin(idUnidadAdmin));");

            db.execSQL("INSERT INTO ArticuloOficina('nombre','cantidad','distribuidor') VALUES('Laptop',2,'Omnisport')");
            db.execSQL("INSERT INTO ArticuloOficina('nombre','cantidad','distribuidor') VALUES('Cañon',3,'Omnisport')");
            db.execSQL("INSERT INTO ArticuloOficina('nombre','cantidad','distribuidor') VALUES('Mouse',5,'Valdez')");

            db.execSQL("INSERT INTO DetalleSustitucion('motivo','descripcion') VALUES('Obsoleto','Equipo sin funcionamiento')");
            db.execSQL("INSERT INTO DetalleSustitucion('motivo','descripcion') VALUES('Incompleto','Equipo incompleto')");
            db.execSQL("INSERT INTO DetalleSustitucion('motivo','descripcion') VALUES('Arruinado','Equipo Arruinado')");

            db.execSQL("INSERT INTO Editorial('nombre','direccion') VALUES('Oceano','prueba1')");
            db.execSQL("INSERT INTO Editorial('nombre','direccion') VALUES('Ceiba','prueba2')");
            db.execSQL("INSERT INTO Editorial('nombre','direccion') VALUES('Latinoamericana','prueba3')");


           /* String[] args = new String[]{"1","Panasonic","PT-LB280E","5","2016/10/10","1"};
            db.execSQL("INSERT INTO Equipo('idUnidadAdmin','marca','modelo','cantidad','fechaCompra','activo') VALUES(?,?,?,?,?,?)",args);
            args = new String[]{"1","Acer","C120","8","2018/1/1","1"};
            db.execSQL("INSERT INTO Equipo('idUnidadAdmin','marca','modelo','cantidad','fechaCompra','activo') VALUES(?,?,?,?,?,?)",args);
            args = new String[]{"2","BenQ","MW705","2","2019/2/12","1"};
            db.execSQL("INSERT INTO Equipo('idUnidadAdmin','marca','modelo','cantidad','fechaCompra','activo') VALUES(?,?,?,?,?,?)",args);

*/
            valor = "Se Guardo Correctamente";

        }catch (SQLException ex){
            valor = "Ocurrio un error";
            Log.i(Tag, "Revisar sintaxis ");
        }
        cerrar();
        return valor;
    }

}
