package edu.ues.fia.eisi.entrega1pdm.CM13106;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ControlBD{
    private static final String Tag = "Mensajes";
    //private static final String[]camposDocumento = new String [] {"idDocumento","titulo","ISBM","idioma", "detalle", "cantidad"};
    //private static final String[]camposAutor = new String [] {"idAutor","nombre","nacionalidad"};
    //private static final String[] camposTipoDocumento = new String [] {"idTipoDoc","catalogo"};

    private static final String[]camposDocumento = new String [] {"idDocumento","idTipoDocumento","idAutor","idEditorial","titulo", "isbm", "idioma","detalle","cantidad"};
    private static final String[]camposAutor = new String [] {"idAutor","nombre","nacionalidad"};
    private static final String[] camposTipoDocumento = new String [] {"idTipoDocumento","catalogo"};

    //db.execSQL("CREATE TABLE documento(idDocumento INTEGER, idAutor INTEGER NOT NULL, idTipoDoc INTEGER NOT NULL, titulo VARCHAR(30),ISBM VARCHAR(30), idioma VARCHAR(30),detalle VARCHAR(50),cantidad INTEGER, PRIMARY KEY(idDocumento, idAutor, idTipoDoc));");
    //db.execSQL("CREATE TABLE autor(idAutor INTEGER NOT NULL PRIMARY KEY,nombre VARCHAR(30), nacionalidad VARCHAR(30));");
    //db.execSQL("CREATE TABLE tipodocumento(idTipoDoc INTEGER NOT NULL PRIMARY KEY,  VARCHAR(50));");

    private static String [] InsertDocumento;
    private static String [] InsertTipoDocumento;
    private static String [] InsertAutor;

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    public Datos datos;
    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }
    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "prestamoFinal.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {

            try{
                //db.execSQL("CREATE TABLE documento(idDocumento INTEGER, idAutor INTEGER NOT NULL, idTipoDoc INTEGER NOT NULL, titulo VARCHAR(30),ISBM VARCHAR(30), idioma VARCHAR(30),detalle VARCHAR(50),cantidad INTEGER, PRIMARY KEY(idDocumento, idAutor, idTipoDoc));");
                //db.execSQL("CREATE TABLE autor(idAutor INTEGER NOT NULL PRIMARY KEY,nombre VARCHAR(30), nacionalidad VARCHAR(30));");
                //db.execSQL("CREATE TABLE tipodocumento(idTipoDoc INTEGER NOT NULL PRIMARY KEY, catalogo VARCHAR(50));");
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// TODO Auto-generated method stub
        }
    }
    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;

    }
    public void cerrar(){
        DBHelper.close();
    }

    public String insertar(Documento documento){
        String regInsertados="Registro Insertado Nº= ";

        long contador=0;
        if(verificarIntegridad(documento,1)){
            //Log.i(Tag, "Entro al INSERT");
        ContentValues doc = new ContentValues();
        doc.put("idDocumento", documento.getIdDocumento());
        doc.put("idTipoDocumento", documento.getIdTipoDoc());
        doc.put("idAutor", documento.getIdAutor());
        doc.put("idEditorial", Integer.parseInt(documento.getDetalle()));
        doc.put("titulo", documento.getTitulo());
        doc.put("isbm", documento.getISBM());
        doc.put("idioma", documento.getIdioma());
        doc.put("cantidad", documento.getCantidad());
        contador=db.insert("Documento", null, doc);
        }
        else {
            //Log.i(Tag, "NO Entro al INSERT");
        }

        if(contador==-1 || contador==0)
        {
            regInsertados= "Error al Insertar el registro, Registro Duplicado. Verificar inserción";
        }
        else {
            regInsertados=regInsertados+contador;
        }
        return regInsertados;

    }
    public String insertar(Autor autor){
        String regInsertados="Registro Insertado Nº= ";
        long contador=0;
        ContentValues aut = new ContentValues();
        aut.put("idAutor", autor.getIdAutor());
        aut.put("nombre", autor.getNombre());
        aut.put("nacionalidad", autor.getNacionalidad());
        contador=db.insert("Autor", null, aut);
        if(contador==-1 || contador==0)
        {
            regInsertados= "Error al Insertar el registro, Registro Duplicado. Verificar inserción";
        }
        else {
            regInsertados=regInsertados+contador;
        }
        return regInsertados;
    }
    public String insertar(TipoDocumento tipoDocumento){
        String regInsertados="Registro Insertado Nº= ";
        long contador=0;
        ContentValues tidoc = new ContentValues();
        tidoc.put("idTipoDocumento", tipoDocumento.getIdTipoDoc());
        tidoc.put("catalogo", tipoDocumento.getCatalogo());
        contador=db.insert("TipoDocumento", null, tidoc);
        if(contador==-1 || contador==0) { regInsertados= "Error al Insertar el registro, Registro Duplicado. Verificar inserción";
        } else {
            regInsertados=regInsertados+contador; } return regInsertados;
    }
    public String actualizar(Documento documento){

        Log.i(Tag, "LLEGO ACTUALIZAR");

        if(verificarIntegridad(documento, 2)){
            Log.i(Tag, "Entro al UPDATE");
            //String[] id = {String.valueOf(documento.getIdDocumento())};
            //ContentValues cv = new ContentValues();
            //cv.put("titulo", documento.getTitulo());
            //cv.put("isbm", documento.getISBM());
            //cv.put("idioma", documento.getIdioma());
            //cv.put("cantidad", documento.getCantidad());
            //cv.put("detalle", documento.getDetalle());
            //db.update("Documento", cv, "idDocumento = ? ", id);



            String[] args = new String[5];
            args[0]=""+documento.getTitulo();
            args[1]=""+documento.getISBM();
            args[2]=""+documento.getIdioma();
            args[3]=""+documento.getCantidad();
            args[4]=""+documento.getIdDocumento();


            try{
                db.execSQL("UPDATE Documento SET titulo =?, isbm =?, idioma =?,cantidad=? WHERE idDocumento = ?",args);
                return "Registro Actualizado Correctamente";
            }catch (SQLException ex){
                return "ERROR";
            }

        }else{
            return "Registro no Existe";
        }
    }
    public String actualizar(TipoDocumento tipoDocumento){
        if(verificarIntegridad(tipoDocumento, 6)){
            String[] id = {String.valueOf(tipoDocumento.getIdTipoDoc())};
            ContentValues cv = new ContentValues();
            cv.put("catalogo", tipoDocumento.getCatalogo());
            db.update("TipoDocumento", cv, "idTipoDocumento = ?", id);
            return "Registro Actualizado Correctamente";
        }else{
            return "Registro con idTipoDocumento " + tipoDocumento.getIdTipoDoc() + " no existe";
        }
    }
    public String actualizar(Autor autor){

        if(verificarIntegridad(autor, 5)){
            String[] id = {String.valueOf(autor.getIdAutor())};
            ContentValues cv = new ContentValues();
            cv.put("nombre", autor.getNombre());
            cv.put("nacionalidad", autor.getNacionalidad());
            db.update("Autor", cv, "idAutor = ?", id);
            return "Registro Actualizado Correctamente";
        }else{
            return "Registro con idAutor " + autor.getIdAutor() + " no existe";
        }
    }
    public String eliminar(Documento documento){
        String regAfectados="filas afectadas= ";
        int contador=0;
        String where="idDocumento='"+documento.getIdDocumento()+"'";
        //where=where+" AND idAutor='"+documento.getIdAutor()+"'";
        //where=where+" AND idTipoDoc="+documento.getIdTipoDoc();
        contador+=db.delete("documento", where, null);
        regAfectados+=contador;
        return regAfectados;
    }
    public String eliminar(TipoDocumento tipoDocumento){
        String regAfectados="filas afectadas= ";
        int contador=0;
        if (verificarIntegridad(tipoDocumento,6)) {
            contador+=db.delete("Documento", "idTipoDocumento='"+tipoDocumento.getIdTipoDoc()+"'", null);
        }
        contador+=db.delete("TipoDocumento", "idTipoDocumento='"+tipoDocumento.getIdTipoDoc()+"'", null);
        regAfectados+=contador;
        return regAfectados;
    }
    public String eliminar(Autor autor){

        String regAfectados="filas afectadas= ";
        int contador=0;
        if (verificarIntegridad(autor,3)) {
            contador+=db.delete("Documento", "idAutor='"+autor.getIdAutor()+"'", null);
        }
        contador+=db.delete("Autor", "idAutor='"+autor.getIdAutor()+"'", null);
        regAfectados+=contador;
        return regAfectados;
    }
    public Documento consultarDocumento(int idDocumento){
        //String[] id = {String.valueOf(idDocumento), String.valueOf(idAutor), String.valueOf(idTipoDoc)};
        String[] args = new String[1];
        args[0]=""+idDocumento;
        Cursor cursor = db.rawQuery("SELECT a.nombre, t.catalogo, e.nombre, " +
                "d.titulo, d.isbm, d.idioma, d.cantidad FROM Documento AS d " +
                "INNER JOIN TipoDocumento AS t USING(idTipoDocumento) " +
                "INNER JOIN Autor as A USING(idAutor) " +
                "INNER JOIN Editorial AS e USING (idEditorial) " +
                "WHERE d.idDocumento = ? ORDER BY t.idTipoDocumento ASC",args);

        if(cursor.moveToFirst()){
            Log.i(Tag, "Entro al SELECT");
            Documento documento = new Documento();
            //documento.setIdDocumento(cursor.getInt(0));

            //documento.setDetalle(cursor.getString(6));
            //documento.setIdAutor(cursor.getInt(1));
            //documento.setIdTipoDoc(cursor.getInt(2));
            documento.setA1(cursor.getString(0));
            documento.setA2(cursor.getString(1));
            documento.setA3(cursor.getString(2));
            documento.setTitulo(cursor.getString(3));
            documento.setISBM(cursor.getString(4));
            documento.setIdioma(cursor.getString(5));
            documento.setCantidad(cursor.getInt(6));
            return documento;
        }else{ return null;
        }
    }
    public TipoDocumento consultarTipoDocumento(int idTipoDoc){

        String[] id = {String.valueOf(idTipoDoc)};
        Cursor cursor = db.query("TipoDocumento", camposTipoDocumento, "idTipoDocumento = ?", id, null, null, null);
        if(cursor.moveToFirst()){
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setIdTipoDoc(Integer.parseInt(cursor.getString(0)));
            tipoDocumento.setCatalogo(cursor.getString(1));
            return tipoDocumento;
        }else{
            return null;
        }
    }
    public Autor consultarAutor(int idAutor){

        String[] id = {String.valueOf(idAutor)};
        Cursor cursor = db.query("Autor", camposAutor, "idAutor = ?", id, null, null, null);
        if(cursor.moveToFirst()){
            Autor autor = new Autor();
            autor.setIdAutor(Integer.parseInt(cursor.getString(0)));
            autor.setNombre(cursor.getString(1));
            autor.setNacionalidad(cursor.getString(2));
             return autor;
        }else{ return null;
        }
    }
    private boolean verificarIntegridad(Object dato, int relacion) throws SQLException{
        switch(relacion){
            case 1:
            {
//verificar que al insertar documento exista autor y el codigo de tipo de documento
                Documento documento = (Documento) dato;
                String[] id1 = {Integer.toString(documento.getIdTipoDoc())};
                String[] id2 = {Integer.toString(documento.getIdAutor())};
                String[] id3 = {documento.getDetalle()};
                String[] id4 = {Integer.toString(documento.getIdDocumento())};
//abrir();
                Cursor cursor1 = db.query("TipoDocumento", null, "idTipoDocumento = ?", id1, null, null, null);
                Cursor cursor2 = db.query("Autor", null, "idAutor = ?", id2, null, null, null);
                Cursor cursor3 = db.query("Editorial", null, "idEditorial = ?", id3, null, null, null);
                Cursor cursor4 = db.query("Documento", null, "idDocumento = ?", id4, null, null, null);

                if(cursor1.moveToFirst() && cursor2.moveToFirst() && cursor3.moveToFirst()){
//Se encontraron datos
                    if (cursor4.moveToFirst()){
                        return false;
                    }else{
                        return true;
                    }
                }
                return false;
            }

            case 2:
            {
                Log.i(Tag, "INTEGRIDAD 2");
//verificar que al modificar documento exista idAutor, el codigo de tipodoc
                Documento documento1 = (Documento) dato;
                //String[] ids = {String.valueOf(documento1.getIdDocumento(), documento1.getIdAutor(), documento1.getIdTipoDoc())};
                String[] ids = {Integer.toString(documento1.getIdDocumento())};
                Cursor c = db.query("Documento", null, "idDocumento = ?", ids, null, null, null);
                if(c.moveToFirst()){
//Se encontraron datos
                    return true;
                }
                return false;
            }
            case 3:
            {
                Autor autor = (Autor)dato;
                Cursor au=db.query(true, "Documento", new String[] {
                        "idAutor" }, "idAutor='"+autor.getIdAutor()+"'",null, null, null, null, null);
                if(au.moveToFirst())
                    return true;
                else
                    return false;
            }
            case 4:
            {
                TipoDocumento tipoDocumento = (TipoDocumento) dato;
                Cursor tdoc=db.query(true, "Documento", new String[] {
                        "codmateria" }, "idTipoDocumento='"+tipoDocumento.getIdTipoDoc()+"'",null, null, null, null, null);
                if(tdoc.moveToFirst())
                    return true;
                else
                    return false;
            }
            case 5:
            {
//verificar que exista autor
                Autor autor2 = (Autor)dato;
                String[] id = {Integer.toString(autor2.getIdAutor())};
                abrir();
                Cursor c2 = db.query("Autor", null, "idAutor = ?", id, null, null, null);
                if(c2.moveToFirst()){
//Se encontro Autor
                    return true;
                }
                return false;
            }
            case 6:
            {
//verificar que exista tipoDocumento
                TipoDocumento tipoDocumento2 = (TipoDocumento) dato;
                String[] idm = {Integer.toString(tipoDocumento2.getIdTipoDoc())};
                abrir();
                Cursor cm = db.query("Tipodocumento", null, "idTipoDocumento = ?", idm, null, null, null);
                if(cm.moveToFirst()){
//Se encontro tipoDocumento
                    return true;
                }
                return false;
            }
            default:
                return false;
        }
    }


    public String llenarBDCarnet(){
        final int[] VAidAutor = {1, 2, 3, 4};
        final String[] VAnombre = {"Juan Perez","Carlos Fidel","Enrique Valencia","Maria Gonzalez"};
        final String[] VAnacionalidad = {"Salvadoreño", "Mexicano", "Estadounidense", "Ingles"};

        final int[] VTidTipoDoc = {1, 2, 3,4 };
        final String[] VTcatalogo = {"Tesis", "Libro", "Reporte", "Libro"};

        final int[] VDidDocumento = {1, 2, 3,4 };
        final int[] VDidAutor = {1, 2, 3, 4};
        final int[] VDidTipoDoc = {1, 2, 3, 4};
        final String[] VDtitulo = {"tecnicas de programacion","enfoque de sistemas","diseño y analisis","consultoria profesional"};
        final String[] VDISBM = {"978-987-1891-20-7","978-987-722-020-9","978-987-1891-21-4","978-987-722-380-4"};
        final String[] VDidioma = {"Ingles","Frances","Español","Ruso"};
        final String[] VDdetalle = {"aprender sobre programacion","hola mundo","tengo suenio","ella se burla de mi"};
        final int[] VDcantidad = {2, 7, 9, 12};
        abrir();
        db.execSQL("DELETE FROM documento");
        db.execSQL("DELETE FROM tipodocumento");
        db.execSQL("DELETE FROM autor");

        Autor autor = new Autor();
        for(int i=0;i<4;i++){
            autor.setIdAutor(VAidAutor[i]);
            autor.setNombre(VAnombre[i]);
            autor.setNacionalidad(VAnacionalidad[i]);
            insertar(autor);
        }
        TipoDocumento tipoDocumento = new TipoDocumento();
        for(int i=0;i<4;i++){
            tipoDocumento.setIdTipoDoc(VTidTipoDoc[i]);
            tipoDocumento.setCatalogo(VTcatalogo[i]);
            insertar(tipoDocumento);
        }
        Documento documento = new Documento();
        for(int i=0;i<4;i++){
            documento.setIdDocumento(VDidDocumento[i]);
            documento.setIdAutor(VDidAutor[i]);
            documento.setIdTipoDoc(VDidTipoDoc[i]);
            documento.setTitulo(VDtitulo[i]);
            documento.setISBM(VDISBM[i]);
            documento.setIdioma(VDidioma[i]);
            documento.setDetalle(VDdetalle[i]);
            documento.setCantidad(VDcantidad[i]);
            insertar(documento);
        }
        cerrar();
        return "Guardo Correctamente";
    }

    public String LlenarBDCarnet() {
        abrir();
        datos = new Datos();

        String cadena;
        db.execSQL("DELETE FROM Documento");
        db.execSQL("DELETE FROM TipoDocumento");
        db.execSQL("DELETE FROM Autor");

        InsertDocumento = datos.getInsertDocumento();
        InsertTipoDocumento = datos.getInsertTipoDocumento();
        InsertAutor = datos.getInsertAutor();
        try {
            for (int i = 0; i < InsertDocumento.length; i++) {

                db.execSQL(InsertDocumento[i]);
            }

            for (int i = 0; i < InsertTipoDocumento.length; i++) {

                db.execSQL(InsertTipoDocumento[i]);
            }

            for (int i = 0; i < InsertAutor.length; i++) {

                db.execSQL(InsertAutor[i]);
            }

        }catch(Exception e){
            e.printStackTrace();
            Log.e("IO","IO"+e);
            return null;
        }
        cerrar();
        cadena = "Guardo Correctamente";
        return cadena;
    }
}
