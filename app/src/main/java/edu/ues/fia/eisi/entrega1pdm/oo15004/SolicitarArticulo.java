package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.Docente;
import edu.ues.fia.eisi.entrega1pdm.R;


public class SolicitarArticulo extends Fragment implements View.OnClickListener{
    Docente d = new Docente();
    int idDocente = d.getIdDocente();

    int idArticulo = 0;
    String nombre="";
    ControladorBD BDhelper;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    private ListView listView;
    FloatingActionButton btnAdd;
    ArrayList<ArticuloOficina> lista = new ArrayList<>();
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BDhelper=new ControladorBD(getActivity());
        v = inflater.inflate(R.layout.fragment_solicitar_articulo, container, false);
        listView = v.findViewById(R.id.listaArticulo);
        btnAdd = v.findViewById(R.id.agregarArticulo);
        btnAdd.setOnClickListener(this);
        btnAdd.setEnabled(false);
        llenarTabla();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                idArticulo = lista.get(index).getIdArticulo();
                nombre = lista.get(index).getNombre();
                btnAdd.setEnabled(true);
            }
        });

        return v;
    }

    private void llenarTabla(){
        lista= BDhelper.listaArticulo();

        stringArrayList= new ArrayList<>();
        for (ArticuloOficina a : lista){
            stringArrayList.add(getString(R.string.listaArticulo)+" "+a.getNombre()+"\n"+getString(R.string.listaCantidad)+" "+a.getCantidad());
        }
        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agregarArticulo:
                ArticuloOficina a = new ArticuloOficina();
                a.setIdArticulo(idArticulo);
                a.setIdDocente(idDocente);
                a.setNombre(nombre);
                Snackbar.make(getView(), BDhelper.solicitarArticulo(a), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                llenarTabla();
                btnAdd.setEnabled(false);
                break;
        }

    }
}
