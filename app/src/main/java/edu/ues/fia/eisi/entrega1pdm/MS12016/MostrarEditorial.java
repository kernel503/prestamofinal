package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import edu.ues.fia.eisi.entrega1pdm.R;


public class MostrarEditorial extends Fragment implements View.OnClickListener{
    private ListView listView;
    private Button btnE_ditar;
    private Button btnE_liminar;
    private ImageButton btnAdd;
    private List<String> stringArrayList ;
    ArrayList<Editorial> lista = new ArrayList<>();
    private static final String Tag = "Mensajes";
    ControlBDMS12016 BDHelper;
    private Integer idEdit;
    private String nomEditorial="";
    private String Direccion="";
    private ArrayAdapter<String> stringArrayAdapter;

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id._add:
                InsertarEditorialFragment fragment = new InsertarEditorialFragment();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;

            case R.id.btnE_ditar:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                Context context = getContext();
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final TextView ed = new TextView(context);
                ed.setText("EDITAR REGISTRO");
                layout.addView(ed);

                final TextView nom = new TextView(context);
                nom.setText("Nombre Editorial");
                layout.addView(nom);

                final EditText nombre = new EditText(context);
                nombre.setText(nomEditorial);
                layout.addView(nombre);

                final TextView dir = new TextView(context);
                dir.setText("Direccion");
                layout.addView(dir);

                final EditText direc = new EditText(context);
                direc.setText(Direccion);
                layout.addView(direc);

                builder.setView(layout);
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tost = "";
                        nomEditorial = nombre.getText().toString();
                        Direccion = direc.getText().toString();
                        Editorial editorial = new Editorial(idEdit,nomEditorial,Direccion);
                        BDHelper.abrir();
                        tost = BDHelper.actualizar(editorial);
                        Toast.makeText(getActivity(), tost, Toast.LENGTH_SHORT).show();
                        BDHelper.cerrar();
                        //  db.abrir();
                        //  db.Actualizar(articulo);
                        // db.cerrar();
                        llenarTabla();

                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

                break;

            case R.id.btnE_liminar:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                builder2.setCancelable(true);
                builder2.setTitle("Confirmar eliminacion");
                builder2.setMessage("Eliminar registro: "+nomEditorial);
                builder2.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String tosta = "";
                                BDHelper.abrir();
                                Editorial e = new Editorial(idEdit,nomEditorial,Direccion);
                                tosta=BDHelper.eliminar(e);
                                BDHelper.cerrar();
                                Toast.makeText(getActivity(),tosta,Toast.LENGTH_SHORT).show();
                                llenarTabla();
                                btnE_ditar.setEnabled(false);
                                btnE_liminar.setEnabled(false);
                            }
                        });
                builder2.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder2.create();
                dialog.show();
                break;

        }//falta eliminar
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mostrar_editorial, container, false);
        BDHelper  = new ControlBDMS12016(getActivity());

        listView=(ListView) v.findViewById(R.id.listView_);
        btnAdd =(ImageButton) v.findViewById(R.id._add);
        btnE_ditar = (Button) v.findViewById(R.id.btnE_ditar);
        btnE_liminar = (Button) v.findViewById(R.id.btnE_liminar);

        btnE_liminar.setOnClickListener(this);
        btnE_ditar.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnE_ditar.setEnabled(false);
        btnE_liminar.setEnabled(false);

        llenarTabla();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);
                btnE_ditar.setEnabled(true);
                btnE_liminar.setEnabled(true);

                idEdit=lista.get(index).getIdEditorial();
                nomEditorial= lista.get(index).getNombre();
                Direccion=lista.get(index).getDireccion();

            }
        });

        return v;
    }


    private void llenarTabla(){
        lista= BDHelper.listaEditorial();
        stringArrayList= new ArrayList<>();
        for (Editorial a : lista){
            stringArrayList.add("Editorial: "+a.getNombre()+" \nDireccion: "+a.getDireccion());
        }

        stringArrayAdapter =new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }


}
