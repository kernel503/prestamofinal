package edu.ues.fia.eisi.entrega1pdm.CV15018;

public class Clasificacion {
    private int idClasificacion;
    private String catalogo;


    public Clasificacion() {

    }


    public int getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(int idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }
}
