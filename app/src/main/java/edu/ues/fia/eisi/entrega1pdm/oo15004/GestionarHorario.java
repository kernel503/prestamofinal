package edu.ues.fia.eisi.entrega1pdm.oo15004;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import edu.ues.fia.eisi.entrega1pdm.R;

public class GestionarHorario extends AppCompatActivity {
    TextView txtHorario;
    Button btnOpc;
    MaterialEditText editdia, editinicio, edifin;
    int opc = 0;
    int idHorario;
    ControladorBD BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar_horario);
        BDhelper=new ControladorBD(this);
        txtHorario = findViewById(R.id.txtHorario);
        btnOpc = findViewById(R.id.btnOpcHorario);
        editdia = findViewById(R.id.editDia);
        editinicio = findViewById(R.id.editInicio);
        edifin = findViewById(R.id.editFin);

        Bundle extras = getIntent().getExtras();

        if(extras !=null)
        {
            opc = extras.getInt("opc");
        }

        if(opc == 0){
            txtHorario.setText("Agregar Horario");
            btnOpc.setText("Agregar");
        }
        else if (opc == 1){
            txtHorario.setText("Editar Horario");
            btnOpc.setText("Editar");
            idHorario = extras.getInt("id");
            editdia.setText( extras.getString("dia"));
            editinicio.setText( extras.getString("inicio"));
            edifin.setText( extras.getString("fin"));
        }
    }
    public void opcion (View v){
        //AGREGAR
        if(opc == 0){
            if (!editinicio.getText().toString().isEmpty() &&
                    !editdia.getText().toString().isEmpty() &&
                    !edifin.getText().toString().isEmpty()){
                Toast.makeText(this,BDhelper.insertarHorario(editdia.getText().toString(),
                        editinicio.getText().toString(),
                        edifin.getText().toString()),Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Ingresar los campos",Toast.LENGTH_SHORT).show();
            }
        }
        //ACTUALIZAR
        if (opc == 1){
            if (!editinicio.getText().toString().isEmpty() &&
                    !editdia.getText().toString().isEmpty() &&
                    !edifin.getText().toString().isEmpty() )
                Toast.makeText(this,BDhelper.actualizarHorario(editdia.getText().toString(),
                        editinicio.getText().toString(),
                        edifin.getText().toString(),idHorario),Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar(View v){
        Intent i = new Intent(getBaseContext(), ContenedorOO15004.class);
        startActivity(i);
    }

}
