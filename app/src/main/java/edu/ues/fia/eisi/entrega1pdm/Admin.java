package edu.ues.fia.eisi.entrega1pdm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.CM13106.MainActivity2;
import edu.ues.fia.eisi.entrega1pdm.CV15018.MainActivityCV15018;
import edu.ues.fia.eisi.entrega1pdm.MS12016.MainActivity3;
import edu.ues.fia.eisi.entrega1pdm.oo15004.ContenedorOO15004;
import edu.ues.fia.eisi.entrega1pdm.oo15004.RegistroPrestamo;

public class Admin extends AppCompatActivity {
    ControladorBDPrincipal BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        BDhelper=new ControladorBDPrincipal(this);
    }

    public void oo15004(View v){
        Intent myIntent = new Intent(Admin.this, ContenedorOO15004.class);
        startActivity(myIntent);
    }

    public void ms12016(View v){
        Intent myIntent = new Intent(Admin.this, MainActivity3.class);
        startActivity(myIntent);
    }

    public void cm13106(View v){
        Intent myIntent = new Intent(Admin.this, MainActivity2.class);
        startActivity(myIntent);
    }

    public void cv15018(View v){
        Intent myIntent = new Intent(Admin.this, MainActivityCV15018.class);
        startActivity(myIntent);
    }

    public void llenarBase (View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Drop Table");
        builder.setMessage("Restaurar valores de las tablas");
        builder.setPositiveButton("Confirmar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getBaseContext(),BDhelper.llenarBase(),Toast.LENGTH_SHORT).show();
                        Toast.makeText(getBaseContext(),BDhelper.CrearTriggers(),Toast.LENGTH_SHORT).show();
                    }
                });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
