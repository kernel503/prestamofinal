package edu.ues.fia.eisi.entrega1pdm.MS12016;

public class ArticuloOficina {
   int  idArticulo;
   String nombre;
   String distribuidor;
   int   cantidad ;
    int idDocente, idPrestamoArticulo;

    public int getIdPrestamoArticulo() {
        return idPrestamoArticulo;
    }

    public void setIdPrestamoArticulo(int idPrestamoArticulo) {
        this.idPrestamoArticulo = idPrestamoArticulo;
    }

    public int getIdDocente() {
        return idDocente;
    }

    public void setIdDocente(int idDocente) {
        this.idDocente = idDocente;
    }

    public ArticuloOficina() {
    }

    public ArticuloOficina(int idArticulo , String nombre, String distribuidor, int cantidad) {
        this.idArticulo = idArticulo;
        this.nombre = nombre;
        this.distribuidor= distribuidor;
        this.cantidad= cantidad;
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        this.distribuidor = distribuidor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}


