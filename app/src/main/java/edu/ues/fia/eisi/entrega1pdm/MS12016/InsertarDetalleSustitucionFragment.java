package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;


public class InsertarDetalleSustitucionFragment extends Fragment implements View.OnClickListener{
    ControlBDMS12016 BDHelper;

    EditText editMotivo;
    EditText editDescripcion;

    Button btnA_gregar;
    Button btnCancelar;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregar:
                String tost = "";
                if (editMotivo.getText().toString().isEmpty() || editDescripcion.getText().toString().isEmpty() ){
                    tost = "LOS CAMPOS ESTAN VACIOS!";
                }else{
                    DetalleSustitucion detalleSustitucion = new DetalleSustitucion();
                    detalleSustitucion.setMotivo(editMotivo.getText().toString());
                    detalleSustitucion.setDescripcion(editDescripcion.getText().toString());

                    BDHelper.abrir();
                    tost= BDHelper.insertarDetalleSustitucion(detalleSustitucion);
                    BDHelper.cerrar();
                    limpiar();
                }
                Toast.makeText(getActivity(),tost,Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnCancelar:
                MostrarDetalleSustitucion fragment = new MostrarDetalleSustitucion();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.Contenedor, fragment);
                tr.commit();
                break;
        }

    }
    public void limpiar(){
        editMotivo.setText("");
        editDescripcion.setText("");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        BDHelper =new ControlBDMS12016(getActivity());
        View v= inflater.inflate(R.layout.fragment_insertar_detalle_sustitucion, container, false);
        editMotivo = (EditText) v.findViewById(R.id.editMotivo);
        editDescripcion = (EditText) v.findViewById(R.id.editDescripcion);
       // editDistribuidor= (EditText) v.findViewById(R.id.editDistribuidor);
        btnA_gregar = (Button)v.findViewById(R.id.btnAgregar);
        btnCancelar= (Button)v.findViewById(R.id.btnCancelar);
        btnA_gregar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        return v;
    }

}
