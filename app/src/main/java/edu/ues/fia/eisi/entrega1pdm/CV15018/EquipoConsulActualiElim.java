package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import edu.ues.fia.eisi.entrega1pdm.R;

public class EquipoConsulActualiElim extends AppCompatActivity {


    EditText etId, editMarca, editModelo, editCantidad, editFecha;
    Spinner spinner, spinnerC;
    ControlBD helper;
    int idUnidadAdmin = 0;
    int idClasificacion = 0;
    //arrayList Para llenar spinner Unidad Administrativa
    ArrayList<String> listToSpinner;
    ArrayList<UnidadAdmin> listaUnidades = new ArrayList<>();

    //arrayList Para llenar spinner de Clasificaciones
    ArrayList<String> listToSpinnerClasif;
    ArrayList<Clasificacion> listaClasificaciones = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_consul_actuali_elim);

        helper = new ControlBD(this);
        etId = (EditText) findViewById(R.id.et_IdE);
        editMarca = (EditText) findViewById(R.id.editMarca);
        editModelo = (EditText) findViewById(R.id.editModelo);
        editCantidad = (EditText) findViewById(R.id.editCantidad);
        editFecha = (EditText) findViewById(R.id.editFecha);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerC = (Spinner) findViewById(R.id.spinnerClasif);

        llenarSpinner();

        //obtener valor/es a usar cuando se selecciona una opccion del spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idUnidadAdmin = listaUnidades.get(position).getIdUnidadAdmin();
                //Toast.makeText(parent.getContext(),"id"+idUnidadAdmin ,Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //spinnerClasificacion
        spinnerC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idClasificacion = listaClasificaciones.get(position).getIdClasificacion();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    //metodo para llenar spinner
    private void llenarSpinner() {
        listaUnidades = helper.listaUnidadAdmin();
        listToSpinner = new ArrayList<>();
        for (UnidadAdmin a : listaUnidades) {
            listToSpinner.add(a.getUbicacion() + " (" + a.getEncargado() + ")");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listToSpinner);
        spinner.setAdapter(adapter);

        //Para SpinnerClasif
        listaClasificaciones = helper.listaClasificacion();
        listToSpinnerClasif = new ArrayList<>();
        for (Clasificacion c : listaClasificaciones) {
            listToSpinnerClasif.add(c.getIdClasificacion() + "-" + c.getCatalogo());
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listToSpinnerClasif);
        spinnerC.setAdapter(adapter2);
    }

    //Metodo para consultar Equipo
    public void consultar(View v) {
        String id = etId.getText().toString();
        if(!id.isEmpty()) {
            helper.abrir();
            Equipo equipo = helper.consultarEquipo(id);
            helper.cerrar();
            if (equipo == null)
                Toast.makeText(this, "Equipo con id " + etId.getText().toString() + " no encontrado", Toast.LENGTH_LONG).show();
            else {
                editMarca.setText(equipo.getMarca());
                editModelo.setText(equipo.getModelo());
                editCantidad.setText(String.valueOf(equipo.getCantidad()));
                editFecha.setText(String.valueOf(equipo.getFechaCompra()));
                int index = indice(spinner, equipo.getIdUnidadAdmin());
                int index2 = indice2(spinnerC, equipo.getIdClasificacion());
                spinner.setSelection(index);
                spinnerC.setSelection(index2);
            }
        }else{
            Toast.makeText(this, "Debe Ingresar el ID para Consultar Registro", Toast.LENGTH_LONG).show();

        }

    }

    //Metodo para Actualizar Equipo
    public void actualizar(View v) {
        Equipo equipo = new Equipo();
        equipo.setId(Integer.parseInt(etId.getText().toString()));
        equipo.setMarca(editMarca.getText().toString());
        equipo.setModelo(editModelo.getText().toString());
        equipo.setCantidad(Integer.parseInt(editCantidad.getText().toString()));
        equipo.setFechaCompra(editFecha.getText().toString());
        equipo.setIdUnidadAdmin(idUnidadAdmin);
        equipo.setIdClasificacion(idClasificacion);

        helper.abrir();
        String estado = helper.actualizarEquipo(equipo);
        helper.cerrar();
        Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
    }

    public void eliminar(View v){
        String id = etId.getText().toString();
        String regEliminadas = "";
        if(!id.isEmpty()) {
            Equipo equipo = new Equipo();
            equipo.setId(Integer.parseInt(id));
            helper.abrir();
            regEliminadas = helper.eliminarEquipo(equipo);
            helper.cerrar();
            Toast.makeText(this, regEliminadas, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Debe Ingresar el id", Toast.LENGTH_SHORT).show();
        }

    }














    public int indice(Spinner spi, int id) {
        int index=0;
        listaUnidades = helper.listaUnidadAdmin();
        String clave = new String();
        for (UnidadAdmin a : listaUnidades) {
            if (a.getIdUnidadAdmin() == id) {
                clave = a.getUbicacion() + " (" + a.getEncargado() + ")";
            }
        }
        for (int i = 1; i < spi.getCount(); i++) {//correcto?
            if (spi.getItemAtPosition(i).toString().equalsIgnoreCase(clave)) {
                index = i;
            }
        }
        return index;
    }
    public int indice2(Spinner spi, int id) {
        int index=0;
        listaClasificaciones = helper.listaClasificacion();
        String clave = new String();
        for (Clasificacion a : listaClasificaciones) {
            if (a.getIdClasificacion() == id) {
                clave = a.getIdClasificacion() + "-" + a.getCatalogo();
            }
        }
        for (int i = 1; i < spi.getCount(); i++) {//correcto?
            if (spi.getItemAtPosition(i).toString().equalsIgnoreCase(clave)) {
                index = i;
            }
        }
        return index;
    }
}
