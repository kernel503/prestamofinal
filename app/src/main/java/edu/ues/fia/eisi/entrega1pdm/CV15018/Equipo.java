package edu.ues.fia.eisi.entrega1pdm.CV15018;

public class Equipo {
    String marca, modelo, fechaCompra;
    int id,cantidad;
    String ubicacion;
    int idUnidadAdmin;
    private int idClasificacion;

    public Equipo(int id,int idUnidadAdmin,String marca, String modelo,int cantidad, String fechaCompra,  String ubicacion) {
        this.marca = marca;
        this.modelo = modelo;
        this.fechaCompra = fechaCompra;
        this.ubicacion = ubicacion;
        this.cantidad = cantidad;
        this.id=id;
        this.idUnidadAdmin=idUnidadAdmin;
    }

    public int getIdUnidadAdmin() {

        return idUnidadAdmin;
    }

    public void setIdUnidadAdmin(int idUnidadAdmin) {

        this.idUnidadAdmin = idUnidadAdmin;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public Equipo() {
    }

    public String getMarca() {

        return marca;
    }

    public void setMarca(String marca) {

        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {

        this.modelo = modelo;
    }

    public String getFechaCompra() {

        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {

        this.fechaCompra = fechaCompra;
    }

    public int getCantidad() {

        return cantidad;
    }

    public void setCantidad(int cantidad) {

        this.cantidad = cantidad;
    }

    public String getUbicacion() {

        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {

        this.ubicacion = ubicacion;
    }

    public int getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(int idClasificacion) {
        this.idClasificacion = idClasificacion;
    }
}
