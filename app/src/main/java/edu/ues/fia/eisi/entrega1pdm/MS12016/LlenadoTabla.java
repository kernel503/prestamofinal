    package edu.ues.fia.eisi.entrega1pdm.MS12016;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;


    public class LlenadoTabla extends Fragment {

        ControlBDMS12016 BDhelper;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            BDhelper=new ControlBDMS12016(getActivity());
            View v= inflater.inflate(R.layout.fragment_llenado_tabla, container, false);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(true);
            builder.setTitle("Drop Table");
            builder.setMessage("Restaurar valores de las tablas");
            builder.setPositiveButton("Confirm",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String tost=BDhelper.llenarBD();
                            Toast.makeText(getActivity(),tost,Toast.LENGTH_SHORT).show();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

            return v;
        }
}
