package edu.ues.fia.eisi.entrega1pdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.ues.fia.eisi.entrega1pdm.oo15004.oo15004Principal;

public class MainActivity extends AppCompatActivity {
    ControladorBDPrincipal BDhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BDhelper=new ControladorBDPrincipal(this);
    }

    public void Redireccionar(View v){

        Intent myIntent = new Intent(MainActivity.this, oo15004Principal.class);

        //myIntent.putExtra("key", value); //Optional parameters
        startActivity(myIntent);
    }
    public void Triggers(View v){
        BDhelper.CrearTriggers();
    }

}
