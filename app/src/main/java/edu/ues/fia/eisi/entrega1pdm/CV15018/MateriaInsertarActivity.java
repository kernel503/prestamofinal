package edu.ues.fia.eisi.entrega1pdm.CV15018;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.ues.fia.eisi.entrega1pdm.R;

public class MateriaInsertarActivity extends AppCompatActivity{

    ControlBD helper;
    EditText et1,et2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_insertar);
        helper = new ControlBD(this);
        et1 = (EditText) findViewById(R.id.et_codMat);
        et2  = (EditText) findViewById(R.id.et_nomMat);

    }

    public void insertar(View v) {
        String cod=et1.getText().toString();
        String nom=et2.getText().toString();

        String regInsertados;
        if(!cod.isEmpty()&&!nom.isEmpty()) {
            Materia materia = new Materia();
            materia.setCodMateria(cod);
            materia.setNombre(nom);
            helper.abrir();
            regInsertados = helper.insertarMateria(materia);
            helper.cerrar();
            Toast.makeText(this, regInsertados, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Debe Conpletar todos los campos", Toast.LENGTH_SHORT).show();

        }

    }


}